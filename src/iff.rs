pub enum AaFileType {
    /// A story file.
    Aavm,
    /// A save file.
    Aasv,
}

impl AaFileType {
    fn as_slice(&self) -> &'static [u8; 4] {
        match self {
            AaFileType::Aavm => b"AAVM",
            AaFileType::Aasv => b"AASV",
        }
    }
}

pub struct AaFileReader<'a> {
    data: &'a [u8],
    cursor: usize,
}

impl<'a> AaFileReader<'a> {
    pub fn from_slice(expected: AaFileType, data: &'a [u8]) -> Option<AaFileReader<'a>> {
        if data.len() <= 12 {
            return None;
        }
        if &data[0..4] != b"FORM" || &data[8..12] != expected.as_slice() {
            return None;
        };
        Some(AaFileReader {
            data,
            cursor: 12, // Start after the type ID of FORM (i.e. AAVM or AASV).
        })
    }
}

impl<'a> Iterator for AaFileReader<'a> {
    type Item = Chunk<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.cursor + 8 > self.data.len() {
            return None;
        }
        let id = &self.data[self.cursor..self.cursor+4];
        self.cursor += 4;
        let length = u32::from_be_bytes(self.data[self.cursor..self.cursor+4].try_into().unwrap());
        self.cursor += 4;
        let contents_start = self.cursor;
        self.cursor += length as usize;
        // If we go beyond the length, simply take the rest of the file.
        // If that happens, it means the IFF file is invalid, but we'll be lenient.
        if self.cursor > self.data.len() {
            self.cursor = self.data.len();
        }
        let contents = &self.data[contents_start..self.cursor];
        self.cursor += self.cursor % 2;
        Some(Chunk {
            id: id.try_into().unwrap(),
            contents,
        })
    }
}

pub struct Chunk<'a> {
    pub id: &'a [u8; 4],
    pub contents: &'a [u8],
}
