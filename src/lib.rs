use std::cmp::Ordering;
use std::collections::{HashMap, HashSet};
use std::io::{self, Write};

use rand::prelude::*;
use rand_pcg::Pcg32;

mod iff;

fn read_u16(slice: &[u8], addr: usize) -> u16 {
    u16::from_be_bytes([slice[addr], slice[addr + 1]])
}

fn push_u16(vec: &mut Vec<u8>, val: u16) {
    vec.extend_from_slice(&val.to_be_bytes());
}

fn push_u32(vec: &mut Vec<u8>, val: u32) {
    vec.extend_from_slice(&val.to_be_bytes());
}

fn write_u32(vec: &mut Vec<u8>, pos: usize, val: u32) {
    let bytes = val.to_be_bytes();
    vec[pos] = bytes[0];
    vec[pos + 1] = bytes[1];
    vec[pos + 2] = bytes[2];
    vec[pos + 3] = bytes[3];
}

fn u16_to_tagged_object(value: u16) -> u16 {
    value & 0x1FFF
}

fn u16_to_tagged_word(value: u16) -> u16 {
    0x2000 | (value & 0x1FFF)
}

fn u16_to_tagged_character(value: u16) -> u16 {
    0x3E00 | (value & 0x00FF)
}

const TAGGED_EMPTY_LIST: u16 = 0x3F00;

fn u16_to_tagged_number(value: u16) -> u16 {
    0x4000 | (value & 0x3FFF)
}

fn u16_to_tagged_reference(value: u16) -> u16 {
    0x8000 | (value & 0x1FFF)
}

fn u16_to_tagged_pair(value: u16) -> u16 {
    0xC000 | (value & 0x1FFF)
}

fn u16_to_tagged_extended_word(value: u16) -> u16 {
    0xE000 | (value & 0x1FFF)
}

#[derive(Debug, PartialEq, Eq)]
enum LiveValueTag {
    Null,
    Object,
    Word,
    Character,
    EmptyList,
    Reserved,
    Number,
    Reference,
    ReservedWithValue,
    Pair,
    ExtendedWord,
}

fn split_tagged_value(value: u16) -> (LiveValueTag, u16) {
    use self::LiveValueTag::*;
    match value {
        0x0000 => (Null, 0), // The value is meaningless here.
        0x0001..=0x1FFF => (Object, value),
        0x2000..=0x3DFF => (Word, value & 0x1FFF),
        0x3E00..=0x3EFF => (Character, value & 0x00FF),
        0x3F00 => (EmptyList, 0), // The value is meaningless here.
        0x3F01..=0x3FFF => (Reserved, 0), // The value is meaningless here.
        0x4000..=0x7FFF => (Number, value & 0x3FFF),
        0x8000..=0x9FFF => (Reference, value & 0x1FFF),
        0xA000..=0xBFFF => (ReservedWithValue, value & 0x1FFF),
        0xC000..=0xDFFE => (Pair, value & 0x1FFF),
        0xE000..=0xFFFE => (ExtendedWord, value & 0x1FFF),
        _ => unreachable!() // TODO: Is it really unreachable?
    }
}

#[derive(Debug)]
pub enum NewAntiopelleError {
    NotAnAaStory,
    MalformedAaStory,
}

#[repr(u8)]
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
enum WhitespaceState {
    Auto = 0,
    NoSpace = 1,
    PendingSpace = 2,
    Space = 3,
    Line = 4,
    Par = 5,
}

impl TryFrom<u8> for WhitespaceState {
    // The only possible error is the u8 is out of range, so we don't really need the error type.
    type Error = ();

    fn try_from(byte: u8) -> Result<Self, Self::Error> {
        use self::WhitespaceState::*;
        Ok(match byte {
            0 => Auto,
            1 => NoSpace,
            2 => PendingSpace,
            3 => Space,
            4 => Line,
            5 => Par,
            _ => return Err(())
        })
    }
}

#[derive(Debug)]
pub struct Style {
    pub reverse: bool,
    pub bold: bool,
    pub italic: bool,
    pub fixed: bool,
}

impl From<u8> for Style {
    fn from(byte: u8) -> Self {
        Style {
            reverse: (byte & 1) != 0,
            bold: (byte & 2) != 0,
            italic: (byte & 4) != 0,
            fixed: (byte & 8) != 0,
        }
    }
}

pub trait Frontend {
    fn supports_hyperlinks(&self) -> bool;
    fn supports_top_status(&self) -> bool;
    fn supports_inline_status(&self) -> bool;
    fn enter_status(&self, area: u16, class: u16);
    fn leave_status(&self);
    fn string(&self, string: &str);
    fn char(&self, ch: char);
    fn space(&self);
    fn space_n(&self, n: u16);
    fn newline(&self);
    fn end_par(&self);
    fn enter_div(&self, class: u16);
    fn leave_div(&self, class: u16);
    fn enter_span(&self, class: u16);
    fn leave_span(&self);
    fn enter_self_link(&mut self);
    fn leave_self_link(&mut self);
    fn enter_link(&mut self, string: &str);
    fn leave_link(&mut self);
    fn enter_link_res(&mut self, res: u16);
    fn leave_link_res(&mut self);
    fn embed_res(&self, res: u16);
    fn progress_bar(&self, current: u16, max: u16);
    fn set_style(&self, style: Style);
    fn reset_style(&self, style: Style);
    fn unstyle(&self);
    fn clear(&self);
    fn clear_all(&self);
    fn clear_links(&self);
    fn clear_div(&self);
    fn clear_old(&self);
    fn leave_all(&mut self);
    fn sync(&self);
    fn script_on(&self) -> bool;
    fn script_off(&self);
    fn can_embed_res(&self, res: u16) -> bool;
    fn trace(&self, msg: &str);
    // The methods `warning` and `error` are not in the spec, but they are useful. (Instead of printing directly with `string` on errors/warnings, we delegate to the frontend.)
    fn warning(&self, msg: &str);
    fn error(&self, msg: &str);
}

pub struct CheapFrontend {
    current_link: Option<String>,
}

impl CheapFrontend {
    pub fn new() -> Self {
        CheapFrontend {
            current_link: None,
        }
    }
}

impl Frontend for CheapFrontend {
    fn supports_hyperlinks(&self) -> bool {
        // We don't really support them, but we display them with brackets.
        // TODO: Add an option in the constructor to disable them?
        true
    }

    fn supports_top_status(&self) -> bool {
        true
    }

    fn supports_inline_status(&self) -> bool {
        true
    }

    fn enter_status(&self, area: u16, class: u16) {
        print!("```");
        if area == 1 {
            print!("inline ");
        }
        println!("status");
    }

    fn leave_status(&self) {
        println!("```");
    }

    fn string(&self, string: &str) {
        print!("{}", string);
    }

    fn char(&self, ch: char) {
        print!("{}", ch);
    }

    fn space(&self) {
        print!(" ");
    }

    fn space_n(&self, n: u16) {
        for _ in 0..n {
            print!(" ");
        }
    }

    fn newline(&self) {
        println!();
    }

    fn end_par(&self) {
        println!("\n");
    }

    fn enter_div(&self, class: u16) {}

    fn leave_div(&self, class: u16) {
        // TODO: Take account of the div's margin.
        println!("\n")
    }

    fn enter_span(&self, class: u16) {}

    fn leave_span(&self) {}

    fn enter_self_link(&mut self) {
        // The current link text isn't important since it's the same as the printed text.
        // But we still need to track the fact that a self-link has been entered.
        self.current_link = Some(String::new());
        print!("[");
    }

    fn leave_self_link(&mut self) {
        self.current_link = None;
        print!("]");
    }

    fn enter_link(&mut self, string: &str) {
        self.current_link = Some(string.to_owned());
        print!("[");
    }

    fn leave_link(&mut self) {
        print!("]({})", self.current_link.as_ref().unwrap());
        self.current_link = None;
    }

    fn enter_link_res(&mut self, res: u16) {
        todo!("CheapFrontend enter_link_res with {}", res);
    }

    fn leave_link_res(&mut self) {
        todo!("CheapFrontend leave_link_res");
    }

    fn embed_res(&self, _: u16) {}

    fn progress_bar(&self, current: u16, max: u16) {
        let count = current * 10 / max;
        println!();
        for _ in 0..count {
            print!("#");
        }
        for _ in 0..10-count {
            print!(".")
        }
        println!()
    }

    fn set_style(&self, _: Style) {}

    fn reset_style(&self, _: Style) {}

    fn unstyle(&self) {}

    fn clear(&self) {}

    fn clear_all(&self) {}

    fn clear_links(&self) {}

    fn clear_div(&self) {}

    fn clear_old(&self) {}

    fn leave_all(&mut self) {
        if let Some(link) = self.current_link.as_ref() {
            if link.is_empty() {
                self.leave_self_link();
            } else {
                self.leave_link()
            }
        }
        self.newline()
    }

    fn sync(&self) {
        io::stdout().flush().unwrap();
    }

    fn script_on(&self) -> bool {
        false
    }

    fn script_off(&self) {}

    fn can_embed_res(&self, _: u16) -> bool {
        false
    }

    fn trace(&self, msg: &str) {
        println!("[trace: {}]", msg);
    }

    fn warning(&self, msg: &str) {
        print!("[WARNING: {}]", msg);
    }

    fn error(&self, msg: &str) {
        print!("[FATAL ERROR: {}]", msg);
    }
}

/// Determines if the DEST operand encoded in the given u8 is a store variant.
fn dest_is_storing(dest: u8) -> bool {
    dest < 0b1000_0000
}

#[derive(Debug)]
enum RuntimeError {
    HeapFull, // 0x01
    AuxHeapFull, // 0x02
    ExpectedObject, // 0x03
    ExpectedBoundValue, // 0x04
    LongTermHeapFull, // 0x06
    InvalidOutputState, // 0x07

    // Below, not strictly-speaking run-time errors,
    // or errors that are not handled by the terp
    // but it's easier to intercept them at the same time.

    Fail,

    Fatal(FatalError)
}

impl From<FatalError> for RuntimeError {
    fn from(e: FatalError) -> Self {
        RuntimeError::Fatal(e)
    }
}

#[derive(Debug)]
enum FatalError {
    /// The interpreter tried to read the code chunk at the given out-of-bound index.
    OutOfBoundCode(u32),
    UnimplementedVmInfo(u8),
    /// An unknown opcode has been encountered.
    ///
    /// The first number is the opcode. The second is its address.
    UnimplementedOpcode(u8, u32),
}

#[derive(Debug, PartialEq, Eq)]
pub enum Status {
    Running,
    AwaitingInput,
    AwaitingKey,
    /// The story is waiting to be saved.
    ///
    /// The vec contains the save file data.
    AwaitingSave(Vec<u8>),
    AwaitingRestore,
    Stopped,
}

/// Represents the pressed key when the story is awaiting a key press.
pub enum Key {
    Return,
    Backspace,
    Up,
    Down,
    Left,
    Right,
    Char(char),
}

type OpcodeResult = Result<(), RuntimeError>;

#[derive(Debug)]
pub struct Antiopelle<IO: Frontend> {
    pub status: Status,
    frontend: IO,
    input_dest: u8,

    // The various chunks of the story.
    head: Vec<u8>,
    code: Vec<u8>,
    dict: Vec<u8>,
    file: Vec<u8>,
    init: Vec<u8>,
    lang: Vec<u8>,
    look: Vec<u8>,
    maps: Vec<u8>,
    meta: Vec<u8>,
    tags: Vec<u8>,
    urls: Vec<u8>,
    writ: Vec<u8>,

    /// Global register used during some recursive operations.
    tmp: u16,

    /// The instruction pointer.
    inst: u32,
    /// The continuation pointer.
    cont: u32,

    /// The main heap top.
    top: u16,
    /// The env frame pointer.
    env: u16,
    /// The choice frame pointer
    cho: u16,
    /// Simple cut or null.
    sim: u16,

    /// The aux stack pointer.
    aux: u16,
    /// The trail stack pointer.
    trl: u16,

    /// The stoppable aux pointer.
    sta: u16,
    /// The stoppable choice pointer.
    stc: u16,

    /// The collect-words level.
    cwl: u8,
    /// The whitespace state.
    spc: WhitespaceState,

    /// The general purpose registers.
    registers: [u16; 64],

    /// The number of objects.
    nob: u16,
    /// The long-term heap bottom.
    ltb: u16,
    /// The long-term heap top.
    ltt: u16,

    // The 2 following are used when decoding strings.
    escape_code_boundary: u16,
    escape_code_size: u16,

    active_divs: Vec<u16>,

    ram: Vec<u16>,

    aux_heap: Vec<u16>,
    heap: Vec<u16>,

    /// Whether or not to print the next character in uppercase.
    uppercase: bool,
    n_span: usize,
    n_link: usize,
    in_status: bool,

    unicode_to_story_map: HashMap<char, (u8, u8)>,
    chars_as_separate_words: HashSet<u8>,
    chars_without_space_before: HashSet<u8>,
    chars_without_space_after: HashSet<u8>,

    initial_state: GameState,
    undo_states: Vec<GameState>,

    trace: bool,

    // The random number generator used by this instance of Antiopelle.
    rng: Pcg32,
}

#[derive(Debug)]
struct GameState {
    inst: u32,
    cont: u32,
    top: u16,
    env: u16,
    cho: u16,
    sim: u16,
    aux: u16,
    trl: u16,
    sta: u16,
    stc: u16,
    cwl: u8,
    spc: WhitespaceState,

    registers: [u16; 64],

    active_divs: Vec<u16>,

    /// Contains the data chunk of a save file.
    /// (NOB, LTB, LTT, the RAM, the auxiliary heap and the heap.)
    data: Vec<u16>,

    ram_len: usize,
    aux_heap_len: usize,
    heap_len: usize,
}

// The "default" state is not a valid game state, but it helps when we need to provide a state temporarily.
impl Default for GameState {
    fn default() -> Self {
        GameState {
            inst: 1,
            cont: 0,
            top: 0,
            env: 0,
            cho: 0,
            sim: 0xFFFF,
            aux: 0,
            trl: 0,
            sta: 0,
            stc: 0,
            cwl: 0,
            spc: WhitespaceState::Line,

            registers: [0; 64],

            active_divs: Vec::new(),

            data: Vec::new(),
            ram_len: 0,
            aux_heap_len: 0,
            heap_len: 0,
        }
    }
}

impl GameState {
    fn from_antiopelle<IO: Frontend>(story: &Antiopelle<IO>, inst: u32) -> GameState {
        let mut data = Vec::with_capacity(
            3 // NOB, LTB and LTT.
            + story.ram.len()
            + story.aux_heap.len()
            + story.heap.len()
        );
        data.push(story.nob);
        data.push(story.ltb);
        data.push(story.ltt);
        data.extend(story.ram.iter().enumerate().map(|(i, &val)| {
            if i < story.ltt as usize {
                val
            } else {
                0x3F3F
            }
        }));
        data.extend(story.aux_heap.iter().enumerate().map(|(i, &val)| {
            if i < story.aux as usize || i >= story.trl as usize {
                val
            } else {
                0x3F3F
            }
        }));
        data.extend(story.heap.iter().enumerate().map(|(i, &val)| {
            if i < story.top as usize || i >= story.env as usize || i >= story.cho as usize {
                val
            } else {
                0x3F3F
            }
        }));

        GameState {
            inst,
            cont: story.cont,
            top: story.top,
            env: story.env,
            cho: story.cho,
            sim: story.sim,
            aux: story.aux,
            trl: story.trl,
            sta: story.sta,
            stc: story.stc,
            cwl: story.cwl,
            spc: story.spc,
            registers: story.registers,
            active_divs: story.active_divs.clone(),
            data,
            ram_len: story.ram.len(),
            aux_heap_len: story.aux_heap.len(),
            heap_len: story.heap.len(),
        }
    }

    // TODO: Make it return a result instead of an option so that we can now the reason the function failed? (Not a save file, a save file for another game...)
    fn from_aasave(save: &[u8], head: &[u8], initial_state: &GameState) -> Option<GameState> {
        let mut state = GameState {
            ram_len: initial_state.ram_len,
            aux_heap_len: initial_state.aux_heap_len,
            heap_len: initial_state.heap_len,
            .. GameState::default()
        };
        let mut got_head = false;
        let mut got_data = false;
        let mut got_regs = false;
        let reader = iff::AaFileReader::from_slice(iff::AaFileType::Aasv, save)?;
        // TODO: We don't check if we encounter a chunk multiple times, so later ones have the final word. Should we check that?
        for iff::Chunk { id, contents } in reader {
            match id {
                b"HEAD" => if contents != head {
                    return None;
                } else {
                    got_head = true;
                }
                b"DATA" => {
                    let mut data = Vec::with_capacity(initial_state.data.len());
                    let mut encoded_stream = contents.iter();
                    let mut initial_stream = initial_state.data.iter()
                        .flat_map(|word| word.to_be_bytes());
                    let mut current_word = [0, 0];
                    let mut current_word_i = 0;
                    while let Some(&encoded_byte) = encoded_stream.next() {
                        if encoded_byte != 0 {
                            let initial_byte = initial_stream.next()?;
                            current_word[current_word_i] = encoded_byte ^ initial_byte;
                            current_word_i = (current_word_i + 1) % current_word.len();
                            if current_word_i == 0 {
                                data.push(u16::from_be_bytes(current_word));
                                current_word = [0, 0];
                            }
                        } else {
                            // If there are not byte after the 0, it means the save file is badly encoded and we return None.
                            let number = *encoded_stream.next()? as usize + 1;
                            for _ in 0..number {
                                let initial_byte = initial_stream.next()?;
                                current_word[current_word_i] = initial_byte;
                                current_word_i = (current_word_i + 1) % current_word.len();
                                if current_word_i == 0 {
                                    data.push(u16::from_be_bytes(current_word));
                                    current_word = [0, 0];
                                }
                            }
                        }
                    }
                    got_data = true;
                }
                b"REGS" => {
                    // All the registers take 156 bytes.
                    if contents.len() < 156 {
                        return None;
                    }
                    let div_number = u16::from_be_bytes(contents[154..156].try_into().unwrap()) as usize;
                    // The chunk should have the number of divs it indicates. (Each div takes 1 word.)
                    if contents.len() < 156 + div_number * 2 {
                        return None;
                    }

                    for (i, reg) in contents[0..64*2]
                        .chunks_exact(2)
                        .map(|bytes| u16::from_be_bytes(bytes.try_into().unwrap()))
                        .enumerate()
                    {
                        state.registers[i] = reg;
                    }
                    state.inst = u32::from_be_bytes(contents[128..132].try_into().unwrap());
                    state.cont = u32::from_be_bytes(contents[132..136].try_into().unwrap());
                    state.top = u16::from_be_bytes(contents[136..138].try_into().unwrap());
                    state.env = u16::from_be_bytes(contents[138..140].try_into().unwrap());
                    state.cho = u16::from_be_bytes(contents[140..142].try_into().unwrap());
                    state.sim = u16::from_be_bytes(contents[142..144].try_into().unwrap());
                    state.aux = u16::from_be_bytes(contents[144..146].try_into().unwrap());
                    state.trl = u16::from_be_bytes(contents[146..148].try_into().unwrap());
                    state.sta = u16::from_be_bytes(contents[148..150].try_into().unwrap());
                    state.stc = u16::from_be_bytes(contents[150..152].try_into().unwrap());
                    state.cwl = contents[152];
                    state.spc = contents[153].try_into().ok()?;
                    state.active_divs = contents[156..156+div_number*2]
                        .chunks_exact(2)
                        .map(|bytes| u16::from_be_bytes(bytes.try_into().unwrap()))
                        .collect();
                    got_regs = true;
                }
                _ => () // Ignore unrecognised chunks.
            }
        }
        if !got_head || !got_data || !got_regs {
            return None;
        }
        Some(state)
    }

    fn nob(&self) -> u16 {
        self.data[0]
    }

    fn ltb(&self) -> u16 {
        self.data[1]
    }

    fn ltt(&self) -> u16 {
        self.data[2]
    }

    fn ram(&self) -> &[u16] {
        &self.data[3..3+self.ram_len]
    }

    fn aux_heap(&self) -> &[u16] {
        let start = 3 + self.ram_len;
        &self.data[start..start+self.aux_heap_len]
    }

    fn heap(&self) -> &[u16] {
        let start = 3 + self.ram_len + self.aux_heap_len;
        &self.data[start..start+self.heap_len]
    }

    fn into_aasave(self, head: &[u8], initial_state: &GameState) -> Vec<u8> {
        let regs_length =
            self.registers.len() * 2 // 1 word for each general-purpose register.
            + 26 // 26 bytes for the special-purpose registers.
            + 2 // One word for the number of divs.
            + self.active_divs.len() * 2; // 1 word for each active div.
        let mut contents = Vec::with_capacity(
            8 // For b"FORM" and its length
            + 4 // For b"AASV"
            + 24 // 4 for each heading and 4 for each of their length (3 headings in total).
            + self.data.len() // We'll allocate too much since the data will be encoded, but it's better than not enough.
            + regs_length // The registers.
            + head.len()
        );

        contents.extend_from_slice(b"FORM");
        let form_length_pos = contents.len();
        push_u32(&mut contents, 0); // The length will be filled at the end.
        contents.extend_from_slice(b"AASV");

        // HEAD.
        contents.extend_from_slice(b"HEAD");
        push_u32(&mut contents, head.len() as u32);
        contents.extend_from_slice(head);
        if contents.len() % 2 != 0 {
            contents.push(0);
        }

        // DATA.
        contents.extend_from_slice(b"DATA");
        let data_length_pos = contents.len();
        push_u32(&mut contents, 0); // The length will be filled at the end.
        let mut n_zeros = 0_u16;
        self.data.iter()
            .flat_map(|&val| val.to_be_bytes())
            .zip(initial_state.data.iter().flat_map(|&val| val.to_be_bytes()))
            .for_each(|(current, initial)| {
                let diff = current ^ initial;
                if diff != 0 {
                    if n_zeros != 0 {
                        contents.push(0);
                        contents.push((n_zeros - 1) as u8);
                        n_zeros = 0;
                    }
                    contents.push(diff);
                } else if n_zeros == 256 {
                    contents.push(0);
                    contents.push(255);
                    n_zeros = 1;
                } else {
                    n_zeros += 1;
                }
            });
        let chunk_length = contents.len() - (data_length_pos + 4);
        write_u32(&mut contents, data_length_pos, chunk_length as u32);
        if contents.len() % 2 != 0 {
            contents.push(0);
        }

        // REGS
        contents.extend_from_slice(b"REGS");
        contents.extend_from_slice(&(regs_length as u32).to_be_bytes());
        contents.extend(self.registers.iter().flat_map(|&reg| reg.to_be_bytes()));
        push_u32(&mut contents, self.inst as u32);
        push_u32(&mut contents, self.cont as u32);
        push_u16(&mut contents, self.top);
        push_u16(&mut contents, self.env);
        push_u16(&mut contents, self.cho);
        push_u16(&mut contents, self.sim);
        push_u16(&mut contents, self.aux);
        push_u16(&mut contents, self.trl);
        push_u16(&mut contents, self.sta);
        push_u16(&mut contents, self.stc);
        contents.push(self.cwl);
        contents.push(self.spc as u8);
        push_u16(&mut contents, self.active_divs.len() as u16);
        contents.extend(self.active_divs.iter().flat_map(|&div| div.to_be_bytes()));
        // `regs_length` is always even, so no need for padding.

        // And finally, the length of the whole FORM chunk.
        let chunk_length = contents.len() - (form_length_pos + 4);
        write_u32(&mut contents, form_length_pos, chunk_length as u32);

        contents
    }
}

impl<IO: Frontend> Antiopelle<IO> {
    /// Creates a new story from an aastory image.
    pub fn from_aastory_image(image: &[u8], frontend: IO) -> Result<Self, NewAntiopelleError> {
        if image.len() < 12 {
            return Err(NewAntiopelleError::NotAnAaStory);
        }

        let mut head = Vec::new();
        let mut code = Vec::new();
        let mut dict = Vec::new();
        let mut file = Vec::new();
        let mut init = Vec::new();
        let mut lang = Vec::new();
        let mut look = Vec::new();
        let mut maps = Vec::new();
        let mut meta = Vec::new();
        let mut tags = Vec::new();
        let mut urls = Vec::new();
        let mut writ = Vec::new();

        let mut nob = 0;
        let mut ltb = 0;
        let mut ltt = 0;

        let reader = iff::AaFileReader::from_slice(iff::AaFileType::Aavm, image).ok_or(NewAntiopelleError::NotAnAaStory)?;

        /* TODO: Check that the HEAD chunk is the first? The spec mandates it, but there doesn't seem to be harm if it's not the case. */

        for iff::Chunk { id, contents } in reader {
            match id {
                b"HEAD" => head = contents.to_vec(),
                b"CODE" => code = contents.to_vec(),
                b"DICT" => dict = contents.to_vec(),
                b"FILE" => file = contents.to_vec(),
                b"INIT" => {
                    init = contents.to_vec();
                    nob = read_u16(&init, 0);
                    ltb = read_u16(&init, 2);
                    ltt = read_u16(&init, 4);
                }
                b"LANG" => lang = contents.to_vec(),
                b"LOOK" => look = contents.to_vec(),
                b"MAPS" => maps = contents.to_vec(),
                b"META" => meta = contents.to_vec(),
                b"TAGS" => tags = contents.to_vec(),
                b"URLS" => urls = contents.to_vec(),
                b"WRIT" => writ = contents.to_vec(),
                _ => ()
            }
        }

        if head.len() < 22 || code.is_empty() {
            return Err(NewAntiopelleError::MalformedAaStory);
        }

        // TODO: Check integrity of all the chunks, and the presence of the mandatory ones.

        // Mapping Unicode chars to the VM-specific chars.
        let mut unicode_to_story_map = HashMap::new();
        let start = read_u16(&lang, 2) as usize;
        let count = lang[start] as usize;
        let start = start + 1;
        for chunk in lang[start..start + count * 5].chunks_exact(5) {
            let code_point = u32::from_be_bytes([0, chunk[2], chunk[3], chunk[4]]);
            let ch = char::from_u32(code_point).unwrap_or(char::REPLACEMENT_CHARACTER);
            unicode_to_story_map.insert(ch, (chunk[0], chunk[1]));
        }

        // Saving the special characters in hashmaps.
        let start = read_u16(&lang, 6) as usize;
        let special_chars = &lang[start..];
        // Characters treated as separate words.
        let count = special_chars[0..].iter()
            .take_while(|&&byte| byte != 0)
            .count();
        let chars_as_separate_words = special_chars[0..count].iter().copied().collect();
        // Characters inhibiting whitespace before.
        let start = count + 1; // Plus 1 to skip the null byte.
        let count = special_chars[start..].iter()
            .take_while(|&&byte| byte != 0)
            .count();
        let chars_without_space_before = special_chars[start..start+count].iter().copied().collect();
        // Characters inhibiting whitespace after.
        let start = count + 1; // Plus 1 to skip the null byte.
        let count = special_chars[start..].iter()
            .take_while(|&&byte| byte != 0)
            .count();
        let chars_without_space_after = special_chars[start..start+count].iter().copied().collect();

        let mut story = Self {
            status: Status::Running,
            frontend,
            input_dest: 0,

            head,
            code,
            dict,
            file,
            init,
            lang,
            look,
            maps,
            meta,
            tags,
            urls,
            writ,

            tmp: 0,

            inst: 1,
            cont: 0,

            top: 0,
            env: 0,
            cho: 0,
            sim: 0xFFFF,

            aux: 0,
            trl: 0,

            sta: 0,
            stc: 0,

            cwl: 0,
            spc: WhitespaceState::Line,

            registers: [0; 64],

            nob,
            ltb,
            ltt,

            escape_code_boundary: 0,
            escape_code_size: 7,

            active_divs: Vec::new(),
            // We don't track the number of active divs, since it's simply `active_divs.len()`.

            ram: Vec::new(),

            aux_heap: Vec::new(),
            heap: Vec::new(),

            uppercase: false,
            n_span: 0,
            n_link: 0,
            in_status: false,

            unicode_to_story_map,
            chars_as_separate_words,
            chars_without_space_before,
            chars_without_space_after,

            initial_state: GameState::default(),
            undo_states: Vec::new(),

            trace: false,

            rng: Pcg32::from_entropy(),
        };

        story.ram.resize(story.ram_size() as usize, 0x3f3f);
        story.aux_heap.resize(story.aux_size() as usize, 0x3F3F);
        story.heap.resize(story.heap_size() as usize, 0x3f3f);

        story.reset(0);
        // TODO: Since `init` is only used here, inline it instead of making it a separate function?
        story.init();

        let (aa_major, aa_minor) = story.aa_version();
        if aa_major == 0 && aa_minor > 3 || aa_major > 0 {
            story.escape_code_boundary = story.extended_char_number().saturating_sub(32) as u16;
            let mut i = story.escape_code_boundary + read_u16(&story.dict, 0) - 1;
            story.escape_code_size = 0;
            while i > 0 {
                i >>= 1;
                story.escape_code_size += 1;
            }
        }

        // Save the initial state for restarts.
        story.initial_state = story.save_state(None);

        Ok(story)
    }

    /// Sets the story's memory to its initial state.
    fn init(&mut self) {
        self.nob = read_u16(&self.init, 0);
        self.ltb = read_u16(&self.init, 2);
        self.ltt = read_u16(&self.init, 4);

        self.heap.fill(0x3F3F);
        self.aux_heap.fill(0x3F3F);
        self.ram[((self.init.len() - 6) / 2)..].fill(0x3F3F);
        for i in (6..self.init.len()).step_by(2) {
            self.ram[(i - 6) / 2] = read_u16(&self.init, i);
        }
    }

    /// Resets the story's registers to their initial state.
    ///
    /// The given argument will be stored in the first entry of the registers.
    fn reset(&mut self, reg0: u16) {
        self.tmp = 0;
        self.inst = 1;
        self.cont = 0;
        self.top = 0;
        self.env = self.heap_size();
        self.cho = self.heap_size();
        self.sim = 0xFFFF;
        self.aux = 0;
        self.trl = self.aux_size();
        self.sta = 0;
        self.stc = 0;
        self.cwl = 0;
        self.spc = WhitespaceState::Line;
        self.registers[0] = reg0;
        self.registers[1..].fill(0);

        self.n_span = 0;
        self.n_link = 0;
        self.in_status = false;
        self.active_divs.clear();
    }

    fn restart(&mut self) {
        // We must temporarily take the initial state not to anger the borrow-checker.
        // TODO: Is that OK?
        let state = std::mem::take(&mut self.initial_state);
        self.restore_state(&state);
        self.initial_state = state;
        self.active_divs.clear();
    }

    /// Saves the current state of the game.
    ///
    /// If `new_inst` contains a value, then this will be the INST saved
    /// instead of the game's current one.
    fn save_state(&self, new_inst: Option<u32>) -> GameState {
        GameState::from_antiopelle(self, new_inst.unwrap_or(self.inst))
    }

    fn restore_state(&mut self, state: &GameState) {
        self.inst = state.inst;
        self.cont = state.cont;
        self.top = state.top;
        self.env = state.env;
        self.cho = state.cho;
        self.sim = state.sim;
        self.aux = state.aux;
        self.trl = state.trl;
        self.sta = state.sta;
        self.stc = state.stc;
        self.cwl = state.cwl;
        self.spc = state.spc;
        self.registers = state.registers;
        self.active_divs = state.active_divs.clone();
        self.nob = state.nob();
        self.ltb = state.ltb();
        self.ltt = state.ltt();
        self.ram = state.ram().to_vec();
        self.aux_heap = state.aux_heap().to_vec();
        self.heap = state.heap().to_vec();

        self.frontend.leave_all();
        self.in_status = false;
        self.n_span = 0;
        self.n_link = 0;
    }

    fn aa_version(&self) -> (u8, u8) {
        (self.head[0], self.head[1])
    }

    fn word_size(&self) -> u8 {
        self.head[2]
    }

    fn shift(&self) -> u8 {
        self.head[3]
    }

    // TODO: Or maybe it should return two u8?
    fn release_number(&self) -> u16 {
        read_u16(&self.head, 4)
    }

    // TODO: Or maybe it should return two u8?
    fn serial_number(&self) -> [u8; 6] {
        self.head[6..12].try_into().unwrap()
    }

    fn crc(&self) -> u32 {
        u32::from_be_bytes(self.head[12..16].try_into().unwrap())
    }

    fn heap_size(&self) -> u16 {
        read_u16(&self.head, 16)
    }

    fn aux_size(&self) -> u16 {
        read_u16(&self.head, 18)
    }

    fn ram_size(&self) -> u16 {
        read_u16(&self.head, 20)
    }

    fn uuid(&self) -> Option<[u8; 45]> {
        if self.head.len() < 68 {
            return None;
        }
        Some(self.head[22..67].try_into().unwrap())
    }

    fn extended_char_number(&self) -> u8 {
        self.lang[read_u16(&self.lang, 2) as usize]
    }

    /// Reads the byte at the instruction pointer in the story's code and advance the instruction pointer.
    fn read_code_byte(&mut self) -> Result<u8, FatalError> {
        let inst_ptr = usize::try_from(self.inst).unwrap();
        let byte = *self.code.get(inst_ptr).ok_or(FatalError::OutOfBoundCode(self.inst))?;
        self.inst += 1;

        Ok(byte)
    }

    /// Gets the byte operand at the instruction pointer.
    fn get_operand_byte(&mut self) -> Result<u8, FatalError> {
        self.read_code_byte()
    }

    /// Gets the vbyte operand at the instruction pointer.
    fn get_operand_vbyte(&mut self) -> Result<u8, FatalError> {
        self.read_code_byte()
    }

    /// Gets the word operand at the instruction pointer.
    fn get_operand_word(&mut self) -> Result<u16, FatalError> {
        let byte_1 = self.read_code_byte()?;
        let byte_2 = self.read_code_byte()?;
        Ok(u16::from_be_bytes([byte_1, byte_2]))
    }

    /// Gets the value operand at the instruction pointer.
    fn get_operand_value(&mut self) -> Result<u16, FatalError> {
        let byte_1 = self.read_code_byte()?;
        if byte_1 & 0b1000_0000 == 0 {
            let byte_2 = self.read_code_byte()?;
            Ok(u16::from_be_bytes([byte_1, byte_2]))
        } else if byte_1 & 0b0100_0000 == 0 {
            Ok(self.registers[(byte_1 & 0b0011_1111) as usize])
        } else {
            Ok(self.heap[self.env as usize + 4 + (byte_1 & 0b0011_1111) as usize])
        }
    }

    /// Get the dest operand at the instruction pointer.
    fn get_operand_dest(&mut self) -> Result<u8, FatalError> {
        self.read_code_byte()
    }

    /// Stores the given value at the destination specified by the operand at the instruction pointer.
    fn store_at_operand_dest(&mut self, dest: u8, value: u16) -> OpcodeResult {
        let addr = dest & 0b0011_1111;
        match dest & 0b1100_0000 {
            0b0000_0000 => self.registers[addr as usize] = value,
            0b0100_0000 => self.heap[self.env as usize + 4 + addr as usize] = value,
            0b1000_0000 => self.unify(value, self.registers[addr as usize])?,
            0b1100_0000 => self.unify(
                value,
                self.heap[self.env as usize + 4 + addr as usize]
            )?,
            _ => unreachable!()
        }
        Ok(())
    }

    /// Gets the index operand at the instruction pointer.
    fn get_operand_index(&mut self) -> Result<u16, FatalError> {
        let byte_1 = self.read_code_byte()?;
        if byte_1 > 0b1100_0000 {
            let byte_1 = byte_1 & 0b0011_1111;
            let byte_2 = self.read_code_byte()?;
            Ok(u16::from_be_bytes([byte_1, byte_2]))
        } else{
            Ok(byte_1 as u16)
        }
    }

    /// Gets the code operand at the instruction pointer.
    fn get_operand_code(&mut self) -> Result<u32, FatalError> {
        let byte_1 = self.read_code_byte()?;
        //Absolute address 0.
        if byte_1 == 0 {
            return Ok(0);
        }

        let flag = byte_1 & 0b1100_0000;
        let value_1 = byte_1 & 0b0011_1111;

        // Close relative pointer.
        if flag == 0 {
            return Ok(self.inst + value_1 as u32);
        }

        // Relative pointer.
        let byte_2 = self.read_code_byte()?;
        if flag == 0b0100_0000 {
            let offset = u32::from_be_bytes([0, 0, value_1, byte_2]);
            if byte_1 & 0b0010_0000 != 0 {
                return Ok(self.inst + offset - 0x4000);
            }
            return Ok(self.inst + offset);
        }

        // Absolute pointer.
        let byte_1 = byte_1 & 0b0111_1111;
        let byte_3 = self.read_code_byte()?;
        Ok(u32::from_be_bytes([0, byte_1, byte_2, byte_3]))
    }

    /// Gets the string operand at the instruction pointer.
    fn get_operand_string(&mut self) -> Result<u32, FatalError> {
        let byte_1 = self.read_code_byte()?;
        if byte_1 < 0x80 {
            return Ok((byte_1 as u32) << 1);
        }
        let byte_2 = self.read_code_byte()?;
        if byte_1 < 0xC0 {
            return Ok(u32::from_be_bytes([0, 0, byte_1 & 0x3F, byte_2]) << self.shift());
        }
        let byte_3 = self.read_code_byte()?;

        Ok(u32::from_be_bytes([0, byte_1 & 0x3f, byte_2, byte_3]) << self.shift())
    }

    /// Execute the opcode at the instruction pointer.
    pub fn step(&mut self) {
        let opcode = match self.read_code_byte() {
            Err(e) => {
                self.fatal_error(e);
                return;
            }
            Ok(op) => op,
        };
        let result = match opcode {
            0x00 => self.op_nop(),
            0x01 => self.op_fail(),
            0x02 => self.op_set_cont(),
            0x03 => self.op_proceed(),
            0x04 => self.op_jmp(),
            0x05 => self.op_jmp_multi(),
            0x85 => self.op_jmpl_multi(),
            0x06 => self.op_jmp_simple(),
            0x86 => self.op_jmpl_simple(),
            0x07 => self.op_jmp_tail(),
            0x87 => self.op_tail(),
            0x08 => self.op_push_env(),
            0x88 => self.op_push_env_0(),
            0x09 => self.op_pop_env(),
            0x89 => self.op_pop_env_proceed(),
            0x0A => self.op_push_choice(),
            0x8A => self.op_push_choice_0(),
            0x0B => self.op_pop_choice(),
            0x8B => self.op_pop_choice_0(),
            0x0C => self.op_pop_push_choice(),
            0x8C => self.op_pop_push_choice_0(),
            0x0D => self.op_cut_choice(),
            0x0E => self.op_get_cho(),
            0x0F => self.op_set_cho(),
            0x10 => self.op_assign_value(),
            0x90 => self.op_assign_vbyte(),
            0x11 => self.op_make_var(),
            0x12 => self.op_make_pair_dest(),
            0x13 => self.op_make_pair_word(),
            0x93 => self.op_make_pair_vbyte(),
            0x14 => self.op_aux_push_val(),
            0x94 => self.op_aux_push_raw_0(),
            0x15 => self.op_aux_push_raw_word(),
            0x95 => self.op_aux_push_raw_vbyte(),
            0x16 => self.op_aux_pop_val(),
            0x17 => self.op_aux_pop_list(),
            0x18 => self.op_aux_pop_list_chk(),
            0x19 => self.op_aux_pop_list_match(),
            0x1B => self.op_split_list(),
            0x1C => self.op_stop(),
            0x1D => self.op_push_stop(),
            0x1E => self.op_pop_stop(),
            0x1F => self.op_split_word(),
            0x9F => self.op_join_words(),
            0x20 => self.op_load_word(),
            0xA0 => self.op_load_word_0(),
            0x21 => self.op_load_byte(),
            0xA1 => self.op_load_byte_0(),
            0x22 => self.op_load_val(),
            0xA2 => self.op_load_val_0(),
            0x24 => self.op_store_word(),
            0xA4 => self.op_store_word_0(),
            0x25 => self.op_store_byte(),
            0xA5 => self.op_store_byte_0(),
            0x26 => self.op_store_val(),
            0xA6 => self.op_store_val_0(),
            0x28 => self.op_set_flag(),
            0xA8 => self.op_set_flag_0(),
            0x29 => self.op_reset_flag(),
            0xA9 => self.op_reset_flag_0(),
            0x2D => self.op_unlink(),
            0xAD => self.op_unlink_0(),
            0x2E => self.op_set_parent_value_value(),
            0xAE => self.op_set_parent_vbyte_value(),
            0x2F => self.op_set_parent_value_vbyte(),
            0xAF => self.op_set_parent_vbyte_vbyte(),
            0x30 => self.op_if_raw_eq(),
            0xB0 => self.op_if_raw_eq_0(),
            0x31 => self.op_if_bound(),
            0x32 => self.op_if_empty(),
            0x33 => self.op_if_num(),
            0x34 => self.op_if_pair(),
            0x35 => self.op_if_obj(),
            0x36 => self.op_if_word(),
            0xB6 => self.op_if_uword(),
            0x37 => self.op_if_unify(),
            0x38 => self.op_if_gt(),
            0x39 => self.op_if_eq_word(),
            0xB9 => self.op_if_eq_vbyte(),
            0x3A => self.op_if_mem_eq_value_value(),
            0xBA => self.op_if_mem_eq_0_value(),
            0x3B => self.op_if_flag(),
            0xBB => self.op_if_flag_0(),
            0x3C => self.op_if_cwl(),
            0x3D => self.op_if_mem_eq_value_vbyte(),
            0xBD => self.op_if_mem_eq_0_vbyte(),
            0x40 => self.op_ifn_raw_eq(),
            0xC0 => self.op_ifn_raw_eq_0(),
            0x41 => self.op_ifn_bound(),
            0x42 => self.op_ifn_empty(),
            0x43 => self.op_ifn_num(),
            0x44 => self.op_ifn_pair(),
            0x45 => self.op_ifn_obj(),
            0x46 => self.op_ifn_word(),
            0xC6 => self.op_ifn_uword(),
            0x47 => self.op_ifn_unify(),
            0x48 => self.op_ifn_gt(),
            0x49 => self.op_ifn_eq_word(),
            0xC9 => self.op_ifn_eq_vbyte(),
            0x4A => self.op_ifn_mem_eq_value_value(),
            0xCA => self.op_ifn_mem_eq_0_value(),
            0x4B => self.op_ifn_flag(),
            0xCB => self.op_ifn_flag_0(),
            0x4C => self.op_ifn_cwl(),
            0x4D => self.op_ifn_mem_eq_value_vbyte(),
            0xCD => self.op_ifn_mem_eq_0_vbyte(),
            0x50 => self.op_add_raw(),
            0xD0 => self.op_inc_raw(),
            0x51 => self.op_sub_raw(),
            0xD1 => self.op_dec_raw(),
            0x52 => self.op_rand_raw(),
            0x58 => self.op_add_num(),
            0xD8 => self.op_inc_num(),
            0x59 => self.op_sub_num(),
            0xD9 => self.op_dec_num(),
            0x5A => self.op_rand_num(),
            0x5B => self.op_mul_num(),
            0x5C => self.op_div_num(),
            0x5D => self.op_mod_num(),
            0x60 => self.op_print_a_str_a(),
            0xE0 => self.op_print_n_str_a(),
            0x61 => self.op_print_a_str_n(),
            0xE1 => self.op_print_n_str_n(),
            0x62 => self.op_nospace(),
            0xE2 => self.op_space(),
            0x63 => self.op_line(),
            0xE3 => self.op_par(),
            0x64 => self.op_space_n(),
            0x65 => self.op_print_val(),
            0x66 => self.op_enter_div(),
            0xE6 => self.op_leave_div(),
            0x67 => self.op_enter_status_0(),
            0xE7 => self.op_leave_status(),
            0x68 => self.op_enter_link_res(),
            0xE8 => self.op_leave_link_res(),
            0x69 => self.op_enter_link(),
            0xE9 => self.op_leave_link(),
            0x6A => self.op_enter_self_link(),
            0xEA => self.op_leave_self_link(),
            0x6B => self.op_set_style(),
            0xEB => self.op_reset_style(),
            0x6C => self.op_embed_res(),
            0xEC => self.op_can_embed_res(),
            0x6D => self.op_progress(),
            0x6E => self.op_enter_span(),
            0xEE => self.op_leave_span(),
            0x6F => self.op_enter_status(),
            0x70 => self.op_ext0(),
            0x72 => self.op_save(),
            0xF2 => self.op_save_undo(),
            0x73 => self.op_get_input(),
            0xF3 => self.op_get_key(),
            0x74 => self.op_vm_info(),
            0x78 => self.op_set_idx(),
            0x79 => self.op_check_eq_word(),
            0xF9 => self.op_check_eq_vbyte(),
            0x7A => self.op_check_gt_eq_word(),
            0xFA => self.op_check_gt_eq_vbyte(),
            0x7B => self.op_check_gt_value(),
            0xFB => self.op_check_gt_byte(),
            0x7C => self.op_check_wordmap(),
            0x7D => self.op_check_eq_2_word(),
            0xFD => self.op_check_eq_2_vbyte(),
            0x7F => self.op_tracepoint(),
            _ => Err(RuntimeError::Fatal(FatalError::UnimplementedOpcode(opcode, self.inst - 1)))
        };
        if let Err(e) = result {
            self.handle_error(e);
        }
    }

    fn fail(&mut self) {
        let cho = self.cho as usize;
        self.inst =
            ((self.heap[cho + 4] as u32) << 16)
            | self.heap[cho + 5] as u32;
    }

    fn field_addr(&self, field: u16, obj: u16) -> Result<u16, RuntimeError> {
        if obj > self.nob {
            Err(RuntimeError::ExpectedObject)
        } else {
            Ok(self.ram[obj as usize] + field)
        }
    }

    fn read_field(&self, field: u16, obj: u16) -> u16 {
        if obj > self.nob {
            0
        } else {
            self.ram[(self.ram[obj as usize] + field) as usize]
        }
    }

    fn deref(&self, a: u16) -> u16 {
        let mut a = a;
        loop {
            let (tag, value) = split_tagged_value(a);
            let value = value as usize;
            if tag != LiveValueTag::Reference || self.heap[value] == 0 {
                break a;
            }
            a = self.heap[value];
        }
    }

    fn unbox_int(&self, v: u16) -> Result<u16, RuntimeError> {
        let v = self.deref(v);
        let (tag, value) = split_tagged_value(v);
        if tag != LiveValueTag::Number {
            return Err(RuntimeError::Fail);
        }
        Ok(value)
    }

    fn box_int(&self, v: u16) -> Result<u16, RuntimeError> {
        if !(0..=0x3FFF).contains(&v) {
            return Err(RuntimeError::Fail);
        }
        Ok(u16_to_tagged_number(v))
    }

    fn runtime_error(&mut self, code: u16) {
        self.restart();
        self.registers[0] = u16_to_tagged_number(code);
    }

    fn fatal_error(&mut self, err: FatalError) {
        use self::FatalError::*;

        let msg = match err {
            OutOfBoundCode(i) => format!("Tried to read the code chunk at index {}, which is out of bound", i),
            UnimplementedVmInfo(i) => format!("Unimplemented VM info {:X}.", i),
            UnimplementedOpcode(opcode, addr) => format!("Unimplemented opcode 0x{:X} at 0x{:X}.", opcode, addr),
        };
        self.frontend.error(&msg);
        self.status = Status::Stopped;
    }

    fn handle_error(&mut self, err: RuntimeError) {
        use self::RuntimeError::*;
        match err {
            Fail => self.fail(),
            HeapFull => self.runtime_error(0x01),
            AuxHeapFull => self.runtime_error(0x02),
            ExpectedObject => self.runtime_error(0x03),
            ExpectedBoundValue => self.runtime_error(0x04),
            LongTermHeapFull => self.runtime_error(0x06),
            InvalidOutputState => self.runtime_error(0x07),
            Fatal(fatal_err) => self.fatal_error(fatal_err),
        }
    }

    fn push_serialized(&mut self, v: u16) -> OpcodeResult {
        use self::LiveValueTag::*;

        let mut v = self.deref(v);
        let (tag, value) = split_tagged_value(v);
        if tag == Pair {
            let mut count = 0;
            let mut tag;
            let mut value = value;
            loop {
                self.push_serialized(u16_to_tagged_reference(value))?;
                count += 1;
                v = self.deref(u16_to_tagged_reference(value + 1));
                let (new_tag, new_value) = split_tagged_value(v);
                tag = new_tag;
                value = new_value;
                if tag == EmptyList {
                    v = 0xC000 + count;
                    break;
                } else if tag != Pair {
                    self.push_serialized(v)?;
                    v = 0xE000 + count;
                    break;
                }
            }
        } else if tag == ExtendedWord {
            self.push_serialized(self.heap[value as usize + 1])?;
            self.push_serialized(self.heap[value as usize])?;
            v = 0x8100;
        } else if tag == Reference {
            v = 0x8000
        }
        if self.aux >= self.trl {
            return Err(RuntimeError::AuxHeapFull);
        }
        self.aux_heap[self.aux as usize] = v;
        self.aux += 1;
        Ok(())
    }

    fn pop_serialized(&mut self) -> Result<u16, RuntimeError> {
        self.aux -= 1;
        let v = self.aux_heap[self.aux as usize];
        Ok(if v == 0x8000 {
            let addr = self.top;
            self.top += 1;
            if self.top > self.env.min(self.cho) {
                return Err(RuntimeError::HeapFull);
            }
            self.heap[addr as usize] = 0;
            u16_to_tagged_reference(addr)
        } else if v == 0x8100 {
            let addr = self.top;
            let addr_usize = addr as usize;
            self.top += 2;
            if self.top > self.env.min(self.cho) {
                return Err(RuntimeError::HeapFull);
            }
            self.heap[addr_usize] = self.pop_serialized()?;
            self.heap[addr_usize + 1] = self.pop_serialized()?;
            u16_to_tagged_extended_word(addr)
        } else if (v & 0xC000) == 0xC000 {
            let mut count = v & 0x1FFF;
            let mut result = if (v & 0x2000) != 0 {
                self.pop_serialized()?
            } else {
                TAGGED_EMPTY_LIST
            };
            while count != 0 {
                count -= 1;
                let addr = self.top;
                let addr_usize = addr as usize;
                self.top += 2;
                if self.top > self.env.min(self.cho) {
                    return Err(RuntimeError::HeapFull);
                }
                self.heap[addr_usize] = self.pop_serialized()?;
                self.heap[addr_usize + 1] = result;
                result = u16_to_tagged_pair(addr);
            }
            result
        } else {
            v
        })
    }

    fn pop_serialized_list(&mut self) -> Result<u16, RuntimeError> {
        let mut list = TAGGED_EMPTY_LIST;
        Ok(loop {
            let v = self.pop_serialized()?;
            if v == 0 {
                break list
            }
            let addr = self.top;
            let addr_usize = addr as usize;
            self.top += 2;
            if self.top > self.env.min(self.cho) {
                return Err(RuntimeError::HeapFull);
            }
            self.heap[addr_usize] = v;
            self.heap[addr_usize + 1] = list;
            list = u16_to_tagged_pair(addr);
        })
    }

    fn push_env(&mut self, arg: u16) -> OpcodeResult {
        let addr = self.env.min(self.cho) - 4 - arg;
        if addr < self.top {
            return Err(RuntimeError::HeapFull);
        }

        let addr_usize = addr as usize;
        self.heap[addr_usize] = self.env;
        self.heap[addr_usize+ 1] = self.sim;
        self.heap[addr_usize + 2] = (self.cont >> 16) as u16;
        self.heap[addr_usize + 3] = (self.cont & 0xFFFF) as u16;

        self.env = addr;
        Ok(())
    }

    fn push_choice(&mut self, arg: u16, next: u32) -> OpcodeResult {
        let addr = self.env.min(self.cho) - 9 - arg;
        if addr < self.top {
            return Err(RuntimeError::HeapFull);
        }

        let addr_usize = addr as usize;
        self.heap[addr_usize] = self.env;
        self.heap[addr_usize + 1] = self.sim;
        self.heap[addr_usize + 2] = (self.cont >> 16) as u16;
        self.heap[addr_usize + 3] = (self.cont & 0xFFFF) as u16;
        self.heap[addr_usize + 4] = (next >> 16) as u16;
        self.heap[addr_usize + 5] = (next & 0xFFFF) as u16;
        self.heap[addr_usize + 6] = self.cho;
        self.heap[addr_usize + 7] = self.top;
        self.heap[addr_usize + 8] = self.trl;
        for i in 0..(arg as usize) {
            self.heap[addr_usize + 9 + i] = self.registers[i]
        }
        self.cho = addr;
        Ok(())
    }

    fn pop_choice(&mut self, arg: u8) {
        let cho_usize = self.cho as usize;
        for i in 0..arg as usize {
            self.registers[i] = self.heap[cho_usize + 9 + i];
        }
        while self.trl < self.heap[cho_usize + 8] {
            self.heap[self.aux_heap[self.trl as usize] as usize] = 0;
            self.trl += 1;
        }
        self.top = self.heap[cho_usize + 7];
        self.cont = ((self.heap[cho_usize + 2] as u32) << 16) | self.heap[cho_usize + 3] as u32;
        self.sim = self.heap[cho_usize + 1];
        self.env = self.heap[cho_usize];
        self.cho = self.heap[cho_usize + 6];
    }

    fn pop_push_choice(&mut self, arg_1: u8, arg_2: u32) {
        let cho_usize = self.cho as usize;
        self.heap[cho_usize + 4] = (arg_2 >> 16) as u16;
        self.heap[cho_usize + 5] = (arg_2 & 0xFFFF) as u16;
        for i in 0..arg_1 as usize {
            self.registers[i] = self.heap[cho_usize + 9 + i];
        }
        while self.trl < self.heap[cho_usize + 8] {
            self.heap[self.aux_heap[self.trl as usize] as usize] = 0;
            self.trl += 1;
        }
        self.top = self.heap[cho_usize + 7];
        self.cont = ((self.heap[cho_usize + 2] as u32) << 16) | self.heap[cho_usize + 3] as u32;
        self.sim = self.heap[cho_usize + 1];
        self.env = self.heap[cho_usize];
    }

    fn dest_value(&self, dest: u8) -> u16 {
        if (dest & 0x40) != 0 {
            self.heap[self.env as usize + 4 + (dest & 0x3F) as usize]
        } else {
            self.registers[(dest & 0x3F) as usize]
        }
    }

    /// Helper used in make_pair, when the first argument is a dest.
    fn make_pair_sub_dest(&mut self, arg: u8, addr: u16) -> OpcodeResult {
        if dest_is_storing(arg) {
            self.heap[addr as usize] = 0;
            self.store_at_operand_dest(arg, u16_to_tagged_reference(addr))?;
        } else {
            self.heap[addr as usize] = self.dest_value(arg);
        }
        Ok(())
    }

    /// Helper used in make_pair, when the first argument is not a dest.
    fn make_pair_sub(&mut self, arg: u16, addr: u16) {
        self.heap[addr as usize] = arg;
    }

    /// Used by the opcode MAKE_PAIR.
    ///
    /// `arg_1` can be a word, vbyte or dest, so its type is the largest of the 3 (u16).
    ///
    /// The first argument should be set to true when `arg_1` is a dest.
    fn make_pair(&mut self, arg_1_is_dest: bool, arg_1: u16, arg_2: u8, arg_3: u8) -> OpcodeResult {
        if dest_is_storing(arg_3) {
            let addr = self.top;
            self.top += 2;
            if self.top > self.env.min(self.cho) {
                return Err(RuntimeError::HeapFull);
            }
            if arg_1_is_dest {
                self.make_pair_sub_dest(arg_1 as u8, addr)?;
            } else {
                self.make_pair_sub(arg_1, addr);
            }
            self.make_pair_sub_dest(arg_2, addr + 1)?;
            self.store_at_operand_dest(arg_3, u16_to_tagged_pair(addr))?;
        } else {
            let a3 = self.deref(self.dest_value(arg_3) as u16);
            let (tag, value) = split_tagged_value(a3);
            if tag == LiveValueTag::Reference {
                let addr = self.top;
                self.top += 2;
                if self.top > self.env.min(self.cho) {
                    return Err(RuntimeError::HeapFull);
                }
                if arg_1_is_dest {
                    self.make_pair_sub_dest(arg_1 as u8, addr)?;
                } else {
                    self.make_pair_sub(arg_1, addr);
                }
                self.make_pair_sub_dest(arg_2, addr + 1)?;
                self.unify(a3, u16_to_tagged_pair(addr))?;
            } else if tag == LiveValueTag::Pair {
                if arg_1_is_dest {
                    self.store_at_operand_dest(arg_1 as u8, u16_to_tagged_reference(value))?;
                } else {
                    self.unify(arg_1, u16_to_tagged_reference(value))?;
                }
                self.store_at_operand_dest(arg_2, u16_to_tagged_reference(value + 1))?;
            } else {
                return Err(RuntimeError::Fail);
            }
        }
        Ok(())
    }

    fn prepend_chars(&mut self, word: u16, list: u16) -> Result<u16, RuntimeError> {
        // TODO: Check if it works correctly.
        let (tag, val) = split_tagged_value(word);
        let string = self.word_to_string(tag, val);
        string.chars().try_rfold(list, |acc, ch| {
            let code = match self.unicode_to_story_map.get(&ch) {
                Some((lower, _)) => *lower,
                None => b'?',
            };
            self.create_pair(u16_to_tagged_character(code as u16), acc)
        })
    }

    fn would_unify(&self, a: u16 , b: u16) -> bool {
        use self::LiveValueTag::*;

        let mut a = a;
        let mut b = b;
        loop {
            a = self.deref(a);
            let (tag_a, val_a) = split_tagged_value(a);
            b = self.deref(b);
            let (tag_b, val_b) = split_tagged_value(b);
            if tag_a == Reference || tag_b == Reference {
                return true;
            } else if tag_a == ExtendedWord && tag_b == ExtendedWord {
                a = self.heap[val_a as usize];
                b = self.heap[val_b as usize];
            } else if tag_a == ExtendedWord {
                a = self.heap[val_a as usize];
            } else if tag_b == ExtendedWord {
                b = self.heap[val_b as usize];
            } else if a == b {
                return true;
            } else if tag_a == Pair && tag_b == Pair {
                if !self.would_unify(
                    u16_to_tagged_reference(val_a),
                    u16_to_tagged_reference(val_b)
                ) {
                    return false;
                }
                a = u16_to_tagged_reference(val_a + 1);
                b = u16_to_tagged_reference(val_b + 1);
            } else {
                return false;
            }
        }
    }

    fn unify(&mut self, a: u16, b: u16) -> OpcodeResult {
        use self::LiveValueTag::*;

        let mut a = a;
        let mut b = b;
        loop {
            a = self.deref(a);
            b = self.deref(b);
            let (tag_a, value_a) = split_tagged_value(a);
            let (tag_b, value_b) = split_tagged_value(b);
            if tag_a == Reference && tag_b == Reference {
                if self.trl <= self.aux {
                    return Err(RuntimeError::AuxHeapFull);
                }
                self.trl -= 1;
                match a.cmp(&b) {
                    Ordering::Less => {
                        self.aux_heap[self.trl as usize] = value_b;
                        self.heap[value_b as usize] = a;
                    }
                    Ordering::Greater => {
                        self.aux_heap[self.trl as usize] = value_a;
                        self.heap[value_a as usize] = b;
                    }
                    Ordering::Equal => ()
                }
                break;
            } else if tag_a == Reference {
                if self.trl <= self.aux {
                    return Err(RuntimeError::AuxHeapFull);
                }
                self.trl -= 1;
                self.aux_heap[self.trl as usize] = value_a;
                self.heap[value_a as usize] = b;
                break;
            } else if tag_b == Reference {
                if self.trl <= self.aux {
                    return Err(RuntimeError::AuxHeapFull);
                }
                self.trl -= 1;
                self.aux_heap[self.trl as usize] = value_b;
                self.heap[value_b as usize] = a;
                break;
            } else if tag_a == ExtendedWord && tag_b == ExtendedWord {
                a = self.heap[value_a as usize];
                b = self.heap[value_b as usize];
            } else if tag_a == ExtendedWord {
                a = self.heap[value_a as usize];
            } else if tag_b == ExtendedWord {
                b = self.heap[value_b as usize];
            } else if a == b {
                break;
            } else if tag_a == Pair && tag_b == Pair {
                self.unify(
                    u16_to_tagged_reference(value_a),
                    u16_to_tagged_reference(value_b)
                )?;
                a = u16_to_tagged_reference(value_a + 1);
                b = u16_to_tagged_reference(value_b + 1);
            } else {
                return Err(RuntimeError::Fail);
            }
        }
        Ok(())
    }

    fn get_long_term(&mut self, v: u16) -> Result<u16, RuntimeError> {
        if v & 0x8000 != 0 {
            self.tmp = v & 0x7FFF;
            self.tmp += self.ram[self.tmp as usize];
            Ok(self.pop_long_term()?)
        } else {
            Ok(v)
        }
    }

    fn pop_long_term(&mut self) -> Result<u16, RuntimeError> {
        self.tmp -= 1;
        let v = self.ram[self.tmp as usize];
        // TODO: This part is in the spec, but not in the JS implementation.
        // if v == 0x8000 {
        //     let addr = self.top;
        //     self.top += 1;
        //     if self.top > self.env.min(self.cho) {
        //         return Err(RuntimeError::HeapFull);
        //     }
        //     self.heap[addr as usize] = 0;
        //     Ok(u16_to_tagged_reference(addr))
        // } else if v == 0x8100 {
        if v == 0x8100 {
            let addr = self.top;
            let addr_usize = addr as usize;
            self.top += 2;
            if self.top > self.env.min(self.cho) {
                return Err(RuntimeError::HeapFull);
            }
            self.heap[addr_usize] = self.pop_long_term()?;
            self.heap[addr_usize + 1] = self.pop_long_term()?;
            Ok(u16_to_tagged_extended_word(addr))
        } else if (v & 0xC000) == 0xC000 {
            let mut count = v & 0x1FFF;
            let mut v = if (v & 0x2000) != 0 {
                self.pop_long_term()?
            } else {
                TAGGED_EMPTY_LIST
            };
            while count != 0 {
                count -= 1;
                let addr = self.top;
                let addr_usize = addr as usize;
                self.top += 2;
                if self.top > self.env.min(self.cho) {
                    return Err(RuntimeError::HeapFull);
                }
                self.heap[addr_usize] = self.pop_long_term()?;
                self.heap[addr_usize + 1] = v;
                v = u16_to_tagged_pair(addr)
            }
            Ok(v)
        } else {
            Ok(v)
        }
    }

    fn store_long_term(&mut self, addr: u16, v: u16) -> OpcodeResult {
        use self::LiveValueTag::*;

        self.clear_long_term(addr);
        let v = self.deref(v);
        let (tag, _) = split_tagged_value(v);
        if matches!(tag, Pair | ExtendedWord | Reference) {
            self.tmp = self.ltt + 2;
            if self.tmp > self.ram_size() {
                return Err(RuntimeError::LongTermHeapFull)
            }
            self.push_long_term(v)?;
            self.ram[addr as usize] = 0x8000 + self.ltt;
            self.ram[self.ltt as usize] = self.tmp - self.ltt;
            self.ram[self.ltt as usize + 1] = addr;
            self.ltt = self.tmp;
        } else {
            self.ram[addr as usize] = v;
        }
        Ok(())
    }

    fn push_long_term(&mut self, v: u16) -> OpcodeResult {
        use self::LiveValueTag::*;

        let v = self.deref(v);
        let (tag, value) = split_tagged_value(v);
        let v = match tag {
            Pair => {
                let mut count = 0;
                let mut v = v;
                loop {
                    // TODO: The commented lines are what is in the spec.
                    // The uncommented version is what is in the JS implementation.
                    // let (_, value) = split_tagged_value(v);
                    self.push_long_term(v - 0x4000)?;
                    // self.push_long_term(self.heap[value as usize])?;
                    count += 1;
                    v = self.deref(v - 0x3FFF);
                    // v = self.deref(self.heap[value as usize + 1]);
                    let (tag, _) = split_tagged_value(v);
                    if tag == EmptyList {
                        break 0xC000 | count;
                    } else if tag != Pair {
                        self.push_long_term(v)?;
                        break 0xE000 | count;
                    }
                }
            }
            ExtendedWord => {
                self.push_long_term(self.heap[value as usize + 1])?;
                self.push_long_term(self.heap[value as usize])?;
                0x8100
            }
            Reference => return Err(RuntimeError::ExpectedBoundValue),
            _ => v
        };
        if self.tmp > self.ram_size() {
            return Err(RuntimeError::LongTermHeapFull);
        }
        self.ram[self.tmp as usize] = v;
        self.tmp += 1;
        Ok(())
    }

    fn clear_long_term(&mut self, addr: u16) {
        let addr = addr as usize;
        let v = self.ram[addr];
        if (v & 0x8000) != 0 {
            self.ram[addr] = 0;
            let v = v & 0x7FFF;
            let size = self.ram[v as usize];
            let size_usize = size as usize;
            for i in (v as usize)..(self.ltt as usize - size_usize) {
                self.ram[i] = self.ram[i + size_usize];
            }
            self.ltt -= size;
            let mut v = v;
            while v < self.ltt {
                let v_usize = v as usize;
                let i = self.ram[v_usize + 1] as usize;
                self.ram[i] -= size;
                v += self.ram[v_usize];
            }
        }
    }

    /// Returns a random number between 0 and 16_383 (the greatest Å-machine number).
    fn random_number(&mut self) -> u16 {
        self.rng.gen_range(0..=16_383)
    }

    fn split_list(&mut self, list: u16, end: u16) -> Result<u16, RuntimeError> {
        use self::LiveValueTag::*;

        let list = self.deref(list);
        let (tag_list, _) = split_tagged_value(list);
        let end = self.deref(end);
        if list == end || tag_list != Pair {
            return Ok(TAGGED_EMPTY_LIST);
        }
        let first = self.top;
        let current = first;
        self.top += 2;
        if self.top > self.env.min(self.cho) {
            return Err(RuntimeError::HeapFull);
        }
        let mut list = list;
        let mut current = current as usize;
        loop {
            let (_, val_list) = split_tagged_value(list);
            let val_list = val_list as usize;
            self.heap[current] = self.heap[val_list];
            list = self.deref(self.heap[val_list]);
            let (tag_list, _) = split_tagged_value(list);
            if list == end || tag_list != Pair {
                break;
            } else {
                self.heap[current + 1] = u16_to_tagged_pair(self.top);
                current = self.top as usize;
                self.top += 2;
                if self.top > self.env.min(self.cho) {
                    return Err(RuntimeError::HeapFull);
                }
            }
        }
        self.heap[current + 1] = TAGGED_EMPTY_LIST;
        Ok(u16_to_tagged_pair(first))
    }

    fn unlink(&mut self, root_addr: u16, field: u16, key: u16) -> OpcodeResult {
        let key = self.deref(key);
        let (tag, value) = split_tagged_value(key);
        if tag != LiveValueTag::Object {
            return Ok(());
        }
        let tail = self.ram[self.field_addr(field, value)? as usize];
        let mut addr = root_addr as usize;
        while self.ram[addr] != 0 {
            if self.ram[addr] == key {
                self.ram[addr] = tail;
                return Ok(());
            }
            let (_, value) = split_tagged_value(self.ram[addr]);
            addr = self.field_addr(field, value)? as usize;
        }
        Ok(())
    }

    fn set_parent(&mut self, arg_1: u16, arg_2: u16) -> OpcodeResult {
        let field_parent = 0;
        let field_child = 1;
        let field_sibling = 2;

        let arg_1 = self.deref(arg_1);
        let arg_2 = self.deref(arg_2);
        let (tag_1, val_1) = split_tagged_value(arg_1);
        let (tag_2, val_2) = split_tagged_value(arg_2);
        if tag_2 != LiveValueTag::Null && (tag_1 != LiveValueTag::Object || tag_2 != LiveValueTag::Object) {
            return Err(RuntimeError::ExpectedObject);
        }
        if tag_1 == LiveValueTag::Object {
            let old_parent = self.ram[self.field_addr(field_parent, val_1)? as usize];
            if old_parent != 0 {
                self.unlink(
                    self.field_addr(field_child, old_parent)?,
                    field_sibling,
                    arg_1
                )?;
            }
            let addr = self.field_addr(field_parent, val_1)? as usize;
            self.ram[addr] = arg_2;
            if tag_2 == LiveValueTag::Object {
                let addr = self.field_addr(field_sibling, val_1)? as usize;
                let addr_2 = self.field_addr(field_child, val_2)? as usize;
                self.ram[addr] = self.ram[addr_2];
                let addr = self.field_addr(field_child, val_2)? as usize;
                self.ram[addr] = arg_1;
            }
        }
        Ok(())
    }

    fn get_object_name(&mut self, number: u16) -> String {
        let addr = read_u16(&self.tags, 2 + number as usize * 2) as usize;
        let name = self.tags[addr..].iter()
            .take_while(|&&byte| byte != 0)
            .map(|&byte| self.decode_char(byte))
            .collect();
        name
    }

    fn value_to_string(&mut self, tag: LiveValueTag, value: u16) -> String {
        use self::LiveValueTag::*;

        match tag {
            Object => {
                // The "#" sign consumes the uppercase.
                self.uppercase = false;
                let mut string = '#'.to_string();
                if !self.tags.is_empty() {
                    string.push_str(&self.get_object_name(value));
                }
                string
            }
            Number => {
                // The first digit from the number consumes the uppercase.
                self.uppercase = false;
                value.to_string()
            },
            Word | ExtendedWord => self.word_to_string(tag, value),
            Reference => {
                // The "$" sign consumes the uppercase.
                self.uppercase = false;
                "$".to_string()
            },
            EmptyList => {
                // The "[" sign consumes the uppercase.
                self.uppercase = false;
                "[]".to_string()
            },
            Pair => {
                // The "[" sign consumes the uppercase.
                self.uppercase = false;
                let value_usize = usize::try_from(value).unwrap();
                let mut needs_space = false;

                let mut string = String::from("[");
                let printed_val = split_tagged_value(self.deref(self.heap[value_usize]));
                string.push_str(&self.value_to_string(printed_val.0, printed_val.1));
                let mut tail = self.deref(self.heap[value_usize + 1]);
                while tail != TAGGED_EMPTY_LIST {
                    let (tag_tail, val_tail) = split_tagged_value(tail);
                    let val_tail_usize = usize::try_from(val_tail).unwrap();
                    if tag_tail == Pair {
                        if needs_space {
                            string.push(' ');
                        }
                        let printed_val = split_tagged_value(self.deref(self.heap[val_tail_usize]));
                        string.push_str(&self.value_to_string(printed_val.0, printed_val.1));
                        needs_space = true;
                        tail = self.deref(self.heap[val_tail_usize + 1]);
                    } else {
                        string.push_str(" | ");
                        string.push_str(&self.value_to_string(tag_tail, val_tail));
                        tail = TAGGED_EMPTY_LIST;
                    }
                }
                string.push(']');
                string
            }
            Null | Reserved | ReservedWithValue => "".to_string(), // Should never happen.
            Character => self.decode_char(value as u8).to_string(),
        }
    }

    fn print_value(&mut self, v: u16) {
        use self::WhitespaceState::*;
        use self::LiveValueTag::*;

        let (tag, value) = split_tagged_value(v);
        if tag == Character {
            let ch = value as u8;
            if self.chars_without_space_before.contains(&ch) && self.spc < NoSpace {
                self.spc = NoSpace;
            }
            if self.spc == Auto || self.spc == PendingSpace {
                self.frontend.space();
            }
            self.frontend.char(self.decode_char(ch));
            self.uppercase = false;
            if self.chars_without_space_after.contains(&ch) {
                self.spc = NoSpace;
            } else {
                self.spc = Auto;
            }
        } else {
            if self.spc == Auto || self.spc == PendingSpace {
                self.frontend.space();
            }
            let string = self.value_to_string(tag, value);
            self.frontend.string(&string);
            self.spc = Auto;
        }
    }

    fn word_to_string(&mut self, tag: LiveValueTag, value: u16) -> String {
        use self::LiveValueTag::*;

        if tag == Word {
            self.dict_word(value)
        } else if tag == ExtendedWord {
            let mut string = String::new();
            let part_1 = self.heap[value as usize];
            let (part_1_tag, part_1_value) = split_tagged_value(part_1);
            if part_1_tag == Pair {
                let mut part_1_tag = part_1_tag;
                let mut part_1_value = part_1_value;
                while part_1_tag == Pair {
                    let (_, ch) = split_tagged_value(self.heap[part_1_value as usize]);
                    string.push(self.decode_char(ch as u8));
                    self.uppercase = false;
                    let part_1 = self.heap[part_1_value as usize + 1];
                    let (x, y) = split_tagged_value(part_1);
                    part_1_tag = x;
                    part_1_value = y;
                }
            } else {
                string.push_str(&self.word_to_string(part_1_tag, part_1_value));
                let part_2 = self.heap[value as usize + 1];
                let (mut part_2_tag, mut part_2_value) = split_tagged_value(part_2);
                while part_2_tag == Pair {
                    let (_, ch) = split_tagged_value(self.heap[part_2_value as usize]);
                    string.push(self.decode_char(ch as u8));
                    self.uppercase = false;
                    let part_2 = self.heap[part_2_value as usize + 1];
                    let (x, y) = split_tagged_value(part_2);
                    part_2_tag = x;
                    part_2_value = y;
                }
        }
            string
        } else {
            "".to_string()
        }
    }

    /// Returns the char corresponding to the given code.
    ///
    /// To make it easier with the borrow-checker, this function takes account
    /// of `self.uppercase`, but does not set it back to false (so that it
    /// doesn't need to borrow `self` mutably). You should not forget to set
    /// `self.uppercase` to false after using this function.
    fn decode_char(&self, code: u8) -> char {
        match code {
            /* Regarding printing reserved characters, instead of using the Unicode replacement character, we could:

            - Print nothing, but that would mean returning an `Option` and so wrangling with `Option`s whenever this function is used.
            - Print an escape sequence, but that would mean returning a string or an iterator instead of a single char.

            Those options seem overkill for something that never happens in regular stories, so we'll just keep it simple. */
            0x00..=0x1F | 0x7F => char::REPLACEMENT_CHARACTER,
            // ASCII characters.
            0x20..=0x7E => if self.uppercase {
                (code as char).to_ascii_uppercase()
            } else {
                code as char
            }
            0x80..=0xFF => {
                let table_addr = read_u16(&self.lang, 2) as usize;
                let code = if self.uppercase {
                    let char_addr = table_addr + 1 + (code as usize - 0x80) * 5;
                    self.lang[char_addr + 1] as usize
                } else {
                    code as usize - 0x80
                };
                let start = table_addr + 1 + code * 5 + 2;
                let char_code = u32::from_be_bytes(
                    [0, self.lang[start], self.lang[start+1], self.lang[start+2]]
                );
                char::from_u32(char_code).unwrap_or(char::REPLACEMENT_CHARACTER)
            }
        }
    }

    /// Returns the dict word number `number` as a string.
    fn dict_word(&mut self, number: u16) -> String {
        let number = number as usize;
        let entry = 2 + number * 3;
        let length = self.dict[entry] as usize;
        let addr = read_u16(&self.dict, entry + 1) as usize;
        // We multiply by 4 for the worst case scenario, when all UTF-8 chars need 4 bytes.
        let mut word = String::with_capacity(length * 4);
        // We decode the first char separately to take account of the potential uppercase.
        word.push(self.decode_char(self.dict[addr]));
        self.uppercase = false;
        word.extend(self.dict[addr+1..addr+length].iter()
            .map(|&byte| self.decode_char(byte)));
        word
    }

    /// Decodes the string at the given address and returns it.
    fn decode_string(&mut self, addr: u32) -> String {
        let mut string = String::new();
        let start = read_u16(&self.lang, 0) as usize;
        let mut current_node = 0;
        let mut byte_addr = usize::try_from(addr).unwrap();
        let mut byte = self.writ[byte_addr];
        let mut bit_number = 8;
        loop {
            if bit_number == 0 {
                bit_number = 7;
                byte_addr += 1;
                byte = self.writ[byte_addr]
            } else {
                bit_number -= 1;
            }
            let bit = byte & (1 << bit_number);
            let action = if bit == 0 {
                self.lang[start + current_node * 2]
            } else {
                self.lang[start + current_node * 2 + 1]
            };
            match action {
                0x00..=0x5E | 0x60..=0x7F => {
                    string.push(self.decode_char(0x20 + action));
                    self.uppercase = false;
                    current_node = 0;
                }
                0x5F => {
                    let mut x = 0_u16;
                    for _ in 0..self.escape_code_size {
                        if bit_number == 0 {
                            bit_number = 7;
                            byte_addr += 1;
                            byte = self.writ[byte_addr];
                        } else {
                            bit_number -= 1;
                        }
                        x <<= 1;
                        let bit = byte & (1 << bit_number);
                        if bit != 0 {
                            x |= 1;
                        }
                    }
                    let (aa_major, aa_minor) = self.aa_version();
                    if aa_major == 0 && aa_minor <= 3 {
                        string.push(self.decode_char(0x80 + x as u8));
                        self.uppercase = false;
                    } else if x < self.escape_code_boundary {
                            string.push(self.decode_char(0xA0 + x as u8));
                            self.uppercase = false;
                    } else {
                        string.push(' ');
                        let word = self.dict_word(x - self.escape_code_boundary);
                        string.push_str(&word);
                    }
                    current_node = 0;
                }
                0x80 => break,
                0x81..=0xFF => current_node = action as usize - 0x80,
            }
        }
        string
    }

    fn create_pair(&mut self, head: u16, tail: u16) -> Result<u16, RuntimeError> {
        let addr = self.top;
        let addr_usize = addr as usize;
        self.top += 2;
        if self.top > self.env.min(self.cho) {
            return Err(RuntimeError::HeapFull);
        }
        self.heap[addr_usize] = head;
        self.heap[addr_usize + 1] = tail;
        Ok(u16_to_tagged_pair(addr))
    }

    fn dict_lookup(&self, word: &[u8]) -> u16 {
        let number = read_u16(&self.dict, 0) as usize;
        // TODO: Use a chunks iterator?
        for i in 0..number {
            // We add 2 to skip the word at the start of the chunk.
            let length = self.dict[2 + i * 3] as usize;
            let addr = read_u16(&self.dict, 2 + i * 3 + 1) as usize;
            if &self.dict[addr..addr+length] == word {
                return i as u16;
            }
        }
        0
    }

    fn parse_word(&mut self, input: &[u8]) -> Result<u16, RuntimeError> {
        if input.len() == 1 {
            if input[0].is_ascii_digit() {
                return Ok(u16_to_tagged_number((input[0] - b'0') as u16));
            } else {
                return Ok(u16_to_tagged_character(input[0] as u16));
            }
        }

        // Look for an exact match in the DICT chunk.
        let d = self.dict_lookup(input);
        if d != 0 {
            return Ok(u16_to_tagged_word(d));
        }

        // Check for a decimal number in the range 0-16383.
        let num = input.iter().enumerate().try_rfold(0_u32, |acc, (place, &byte)| {
            if byte.is_ascii_digit() {
                let num = acc + (byte - b'0') as u32 * 10_u32.pow(u32::try_from(place).unwrap());
                if num > 16_383 {
                    None // Overflow.
                } else {
                    Some(num)
                }
            } else {
                None // Not a number.
            }
        });
        if let Some(num) = num {
            return Ok(u16_to_tagged_number(num as u16));
        }

        // Otherwise, run the word-ending decoder.
        let decoder_addr = read_u16(&self.lang, 4) as usize;
        let decoder = &self.lang[decoder_addr..];
        let mut state = 0;
        let mut ending = TAGGED_EMPTY_LIST;
        let mut pos = input.len();
        loop {
            if decoder[state] == 0x00 {
                while pos > 0 {
                    let addr = self.top;
                    let addr_usize = addr as usize;
                    self.top += 2;
                    if self.top > self.env.min(self.cho) {
                        return Err(RuntimeError::HeapFull);
                    }
                    pos -= 1;
                    let c = input[pos];
                    let v = if (b'0'..b'9').contains(&c) {
                        u16_to_tagged_number((c - b'0') as u16)
                    } else {
                        u16_to_tagged_character(c as u16)
                    };
                    self.heap[addr_usize] = v;
                    self.heap[addr_usize + 1] = ending;
                    ending = u16_to_tagged_pair(addr);
                }
                let addr = self.top;
                let addr_usize = addr as usize;
                self.top += 2;
                if self.top > self.env.min(self.cho) {
                    return Err(RuntimeError::HeapFull);
                }
                self.heap[addr_usize] = ending;
                self.heap[addr_usize + 1] = TAGGED_EMPTY_LIST;
                return Ok(u16_to_tagged_extended_word(addr));
            } else if decoder[state] == 0x01 {
                let d = self.dict_lookup(&input[..pos]);
                if d != 0 {
                    // The first `pos` characters exactly match a dictionary word.
                    let addr = self.top;
                    let addr_usize = addr as usize;
                    self.top += 2;
                    if self.top > self.env.min(self.cho) {
                        return Err(RuntimeError::HeapFull);
                    }
                    self.heap[addr_usize] = u16_to_tagged_word(d);
                    self.heap[addr_usize] = ending;
                    return Ok(u16_to_tagged_extended_word(addr));
                } else {
                    state += 1;
                }
            } else {
                let ch = input[pos - 1];
                if ch == decoder[state] {
                    let addr = self.top;
                    let addr_usize = addr as usize;
                    self.top += 2;
                    if self.top > self.env.min(self.cho) {
                        return Err(RuntimeError::HeapFull);
                    }
                    self.heap[addr_usize] = u16_to_tagged_character(ch as u16);
                    self.heap[addr_usize + 1] = ending;
                    ending = u16_to_tagged_pair(addr);
                    state = decoder[state + 1] as usize;
                    pos -= 1;
                } else {
                    state += 2;
                }
            }
        }
    }

    fn wordlist_to_string(&self, list: u16) -> Result<Vec<u8>, RuntimeError> {
        use self::LiveValueTag::*;

        // TODO: create the vec with some predetermined capacity?
        let mut string = Vec::new();
        let mut list = list;
        let (mut tag_list, val_list) = split_tagged_value(list);
        let mut val_list = val_list as usize;
        loop {
            let v = self.deref(self.heap[val_list]);
            let (tag_v, val_v) = split_tagged_value(v);
            let val_v = val_v as usize;
            match tag_v {
                Character => {
                    let ch = val_v as u8;
                    if ch <= 0x20 || self.chars_as_separate_words.contains(&ch) {
                        return Err(RuntimeError::Fail)
                    }
                    string.push(ch);
                }
                Word => {
                    let entry = self.dict[2 + 3 * val_v] as usize;
                    let length = self.dict[entry] as usize;
                    let addr = read_u16(&self.dict, entry + 1) as usize;
                    string.extend_from_slice(&self.dict[addr..addr+length])
                }
                ExtendedWord => {
                    let part_1 = self.heap[val_v];
                    let (tag_1, _) = split_tagged_value(part_1);
                    if tag_1 == Pair {
                        string.extend_from_slice(&self.wordlist_to_string(part_1)?);
                    } else {
                        // Reinterpret the extended word cell as a list node..
                        string.extend_from_slice(&self.wordlist_to_string(v)?);
                    }
                }
                Number => {
                    // As bytes is OK since digits are ASCII.
                    // Minus 0x20 because that'S how ASCII chars are stored.
                    string.extend(val_v.to_string().as_bytes().iter().map(|&ch| ch - 0x20));
                }
                _ => return Err(RuntimeError::Fail)
            }

            list = self.deref(self.heap[val_list + 1]);
            let (new_tag_list, new_val_list) = split_tagged_value(list);
            tag_list = new_tag_list;
            val_list = new_val_list as usize;
            if tag_list != Pair {
                break;
            }
        }
        if list != TAGGED_EMPTY_LIST {
            return Err(RuntimeError::Fail);
        }
        Ok(string)
    }

    pub fn send_input(&mut self, input: &str) {
        if self.status != Status::AwaitingInput {
            // TODO: return an error?
            return;
        }
        self.status = Status::Running;

        // Convert input string to the story-specific encoding, while lowercasing it.
        let input = input.chars().map(|ch| {
            let code = ch as u32;
            if (0x41..=0x5A).contains(&code) { // Uppercase ASCII letters to lowercase.
                code as u8 + 0x20
            } else if code < 0x80 { // Other ASCII characters are left as is.
                code as u8
            } else { // Otherwise, get character from game-specific set.
                let codes = self.unicode_to_story_map.get(&ch);
                match codes {
                    Some((lower, _)) => *lower,
                    None => 0x3f, // ASCII for '?' if not present.
                }
            }
        })
        .collect::<Vec<_>>();

        // Tokenise the input.
        let mut words = Vec::new();
        let mut start = 0; // The start of the word being scanned.
        for (i, &code) in input.iter().enumerate() {
            if code == 32 { // A space.
                if i != start {
                    words.push(&input[start..i]); // Push word so far.
                }
                start = i + 1;
            } else if self.chars_as_separate_words.contains(&code) {
                    if i != start {
                        words.push(&input[start..i]); // Push word so far.
                    }
                    words.push(&input[i..i+1]); // Push character considered as separate word.
                    start = i + 1;
            }
        }
        if start != input.len() {
            words.push(&input[start..]) // Push the last word.
        }

        // Convert the tokens into list of Aa-machine values.
        let list = words.iter().rev().try_fold(TAGGED_EMPTY_LIST, |acc, &word| {
            let val = self.parse_word(word);
            self.create_pair(val?, acc)
        });
        let list = match list {
            Err(e) => {
                self.handle_error(e);
                return;
            }
            Ok(list) => list
        };

        // And now the "end" of the get_input opcode.
        let result = self.store_at_operand_dest(self.input_dest, list);
        if let Err(e) = result {
            self.handle_error(e);
            return;
        }
        self.spc = WhitespaceState::Line;
    }

    /// Sends a key press when the story is awaiting one.
    ///
    /// If the function returns false, the key was not recognised by the story and we should keep waiting for one.
    pub fn send_key(&mut self, key: Key) -> bool {
        if self.status != Status::AwaitingKey {
            // TODO: Maybe return true? Or return an error?
            return false;
        }

        let value = match key {
            Key::Backspace => 0x08,
            Key::Return => 0x0D,
            Key::Up => 0x10,
            Key::Down => 0x11,
            Key::Left => 0x12,
            Key::Right => 0x13,
            Key::Char(ch) => if ch.is_ascii() && !ch.is_ascii_control() {
                // ASCII printable characters.
                ch.to_ascii_lowercase() as u8
            } else if let Some((lower, _)) = self.unicode_to_story_map.get(&ch) {
                *lower
            } else {
                // Unrecognised character, keep waiting for a key.
                return false;
            }
        };

        // Convert the value to a tagged number of character.
        let value = if value.is_ascii_digit() {
            u16_to_tagged_number(value as u16)
        } else {
            u16_to_tagged_character(value as u16)
        };

        self.status = Status::Running;
        // And now the "end" of the get_key opcode.
        let result = self.store_at_operand_dest(self.input_dest, value);
        if let Err(e) = result {
            self.handle_error(e);
        }
        true
    }

    /// Tells the story that we handled the save.
    ///
    /// Passing true means the save succeeded, and passing false means it failed.
    pub fn send_save_result(&mut self, result: bool) {
        match self.status {
            Status::AwaitingSave(_) => (), // Do nothing and proceed.
            _ => return // TODO: return an error?
        }
        self.status = Status::Running;

        // And now the "end" of the save opcode.
        if !result {
            self.handle_error(RuntimeError::Fail);
        }
    }


    pub fn send_restore_result(&mut self, data: Option<&[u8]>) {
        if self.status != Status::AwaitingRestore {
            // TODO: return an error?
            return;
        }
        self.status = Status::Running;

        // And now the "end" of the restore branch of the ext0 opcode.
        if let Some(data) = data {
            if let Some(state) = GameState::from_aasave(data, &self.head, &self.initial_state) {
                self.restore_state(&state);
                for &div in &self.active_divs {
                    self.frontend.enter_div(div);
                }
            }
        }
        // Else restoring failed (e.g. the player cancelled the open dialog or the file was invalid) and do nothing.
    }

    // Opcode 0x00.
    fn op_nop(&mut self) -> OpcodeResult {
        // Do nothing.
        Ok(())
    }

    // Opcode 0x01.
    fn op_fail(&mut self) -> OpcodeResult {
        Err(RuntimeError::Fail)
    }

    // Opcode 0x02.
    fn op_set_cont(&mut self) -> OpcodeResult {
        self.cont = self.get_operand_code()?;
        Ok(())
    }

    // Opcode 0x03.
    fn op_proceed(&mut self) -> OpcodeResult {
        if self.sim < 0x8000 {
            self.cho = self.sim;
        }
        self.inst = self.cont;
        Ok(())
    }

    // Opcode 0x04.
    fn op_jmp(&mut self) -> OpcodeResult {
        let arg = self.get_operand_code()?;
        self.inst = arg;
        Ok(())
    }

    // Opcode 0x05.
    fn op_jmp_multi(&mut self) -> OpcodeResult {
        let arg = self.get_operand_code()?;
        self.sim = 0xFFFF;
        self.inst = arg;
        Ok(())
    }

    // Opcode 0x85.
    fn op_jmpl_multi(&mut self) -> OpcodeResult {
        let arg = self.get_operand_code()?;
        self.cont = self.inst;
        self.sim = 0xFFFF;
        self.inst = arg;
        Ok(())
    }

    // Opcode 0x06.
    fn op_jmp_simple(&mut self) -> OpcodeResult {
        let arg = self.get_operand_code()?;
        self.sim = self.cho;
        self.inst = arg;
        Ok(())
    }

    // Opcode 0x86.
    fn op_jmpl_simple(&mut self) -> OpcodeResult {
        let arg = self.get_operand_code()?;
        self.cont = self.inst;
        self.sim = self.cho;
        self.inst = arg;
        Ok(())
    }

    // Opcode 0x07.
    fn op_jmp_tail(&mut self) -> OpcodeResult {
        let arg = self.get_operand_code()?;
        if self.sim >= 0x8000 {
            self.sim = self.cho;
        }
        self.inst = arg;
        Ok(())
    }

    // Opcode 0x87.
    fn op_tail(&mut self) -> OpcodeResult {
        if self.sim >= 0x8000 {
            self.sim = self.cho;
        }
        Ok(())
    }

    // Opcode 0x08.
    fn op_push_env(&mut self) -> OpcodeResult {
        let arg = self.get_operand_byte()? as u16;
        self.push_env(arg)
    }

    // Opcode 0x88.
    fn op_push_env_0(&mut self) -> OpcodeResult {
        self.push_env(0)
    }

    // Opcode 0x09.
    fn op_pop_env(&mut self) -> OpcodeResult {
        let env = self.env as usize;
        self.cont =
            ((self.heap[env + 2] as u32) << 16)
            | self.heap[env + 3] as u32;
        self.sim = self.heap[env + 1];
        self.env = self.heap[env];
        Ok(())
    }

    // Opcode 0x89.
    fn op_pop_env_proceed(&mut self) -> OpcodeResult {
        let env = self.env as usize;
        self.inst =
            ((self.heap[env + 2] as u32) << 16)
            | self.heap[env + 3] as u32;
        if self.heap[env + 1] < 0x8000 {
            self.cho = self.heap[env + 1];
        }
        self.env = self.heap[env];
        Ok(())
    }

    // Opcode 0x0A.
    fn op_push_choice(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_byte()? as u16;
        let arg_2 = self.get_operand_code()?;
        self.push_choice(arg_1, arg_2)
    }

    // Opcode 0x8A.
    fn op_push_choice_0(&mut self) -> OpcodeResult {
        let arg = self.get_operand_code()?;
        self.push_choice(0, arg)
    }

    // Opcode 0x0B.
    fn op_pop_choice(&mut self) -> OpcodeResult {
        let arg = self.get_operand_byte()?;
        self.pop_choice(arg);
        Ok(())
    }

    // Opcode 0x8B.
    fn op_pop_choice_0(&mut self) -> OpcodeResult {
        self.pop_choice(0);
        Ok(())
    }

    // Opcode 0x0C.
    fn op_pop_push_choice(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_byte()?;
        let arg_2 = self.get_operand_code()?;
        self.pop_push_choice(arg_1, arg_2);
        Ok(())
    }

    // Opcode 0x8C.
    fn op_pop_push_choice_0(&mut self) -> OpcodeResult {
        let arg = self.get_operand_code()?;
        self.pop_push_choice(0, arg);
        Ok(())
    }

    // Opcode 0x0D.
    fn op_cut_choice(&mut self) -> OpcodeResult {
        self.cho = self.heap[self.cho as usize + 6];
        Ok(())
    }

    // Opcode 0x0E.
    fn op_get_cho(&mut self) -> OpcodeResult {
        let arg = self.get_operand_dest()?;
        self.store_at_operand_dest(arg, self.cho)
    }

    // Opcode 0x0F.
    fn op_set_cho(&mut self) -> OpcodeResult {
        let arg = self.get_operand_value()?;
        self.cho = arg;
        Ok(())
    }

    // Opcode 0x10.
    fn op_assign_value(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_dest()?;
        self.store_at_operand_dest(arg_2, arg_1)
    }

    // Opcode 0x90.
    fn op_assign_vbyte(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_vbyte()? as u16;
        let arg_2 = self.get_operand_dest()?;
        self.store_at_operand_dest(arg_2, arg_1)
    }

    // Opcode 0x11.
    fn op_make_var(&mut self) -> OpcodeResult {
        let arg = self.get_operand_dest()?;
        let addr = self.top;
        self.top += 1;
        if self.top > self.env.min(self.cho) {
            return Err(RuntimeError::HeapFull);
        }
        self.heap[addr as usize] = 0;
        self.store_at_operand_dest(arg, u16_to_tagged_reference(addr))
    }

    // Opcode 0x12.
    fn op_make_pair_dest(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_dest()?;
        let arg_2 = self.get_operand_dest()?;
        let arg_3 = self.get_operand_dest()?;
        self.make_pair(true, arg_1 as u16, arg_2, arg_3)
    }

    // Opcode 0x13.
    fn op_make_pair_word(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_word()?;
        let arg_2 = self.get_operand_dest()?;
        let arg_3 = self.get_operand_dest()?;
        self.make_pair(false, arg_1, arg_2, arg_3)
    }

    // Opcode 0x93.
    fn op_make_pair_vbyte(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_vbyte()? as u16;
        let arg_2 = self.get_operand_dest()?;
        let arg_3 = self.get_operand_dest()?;
        self.make_pair(false, arg_1, arg_2, arg_3)
    }

    // Opcode 0x14.
    fn op_aux_push_val(&mut self) -> OpcodeResult {
        let arg = self.get_operand_value()?;
        self.push_serialized(arg)
    }

    // Opcode 0x94.
    fn op_aux_push_raw_0(&mut self) -> OpcodeResult {
        if self.aux >= self.trl {
            return Err(RuntimeError::AuxHeapFull);
        }
        self.aux_heap[self.aux as usize] = 0;
        self.aux += 1;
        Ok(())
    }

    // Opcode 0x15.
    fn op_aux_push_raw_word(&mut self) -> OpcodeResult {
        let arg = self.get_operand_word()?;
        if self.aux >= self.trl {
            return Err(RuntimeError::AuxHeapFull);
        }
        self.aux_heap[self.aux as usize] = arg;
        self.aux += 1;
        Ok(())
    }

    // Opcode 0x95.
    fn op_aux_push_raw_vbyte(&mut self) -> OpcodeResult {
        let arg = self.get_operand_vbyte()? as u16;
        if self.aux >= self.trl {
            return Err(RuntimeError::AuxHeapFull);
        }
        self.aux_heap[self.aux as usize] = arg;
        self.aux += 1;
        Ok(())
    }

    // Opcode 0x16.
    fn op_aux_pop_val(&mut self) -> OpcodeResult {
        let arg = self.get_operand_dest()?;
        let value = self.pop_serialized()?;
        self.store_at_operand_dest(arg, value)
    }

    // Opcode 0x17.
    fn op_aux_pop_list(&mut self) -> OpcodeResult {
        let arg = self.get_operand_dest()?;
        let value = self.pop_serialized_list()?;
        self.store_at_operand_dest(arg, value)
    }

    // Opcode 0x18.
    fn op_aux_pop_list_chk(&mut self) -> OpcodeResult {
        let arg = self.get_operand_value()?;
        let mut flag = false;
        let key = self.deref(arg);
        loop {
            self.aux -= 1;
            let v = self.aux_heap[self.aux as usize];
            if v == 0 {
                break;
            }
            if v == key {
                flag = true;
            }
        }

        if !flag {
            return Err(RuntimeError::Fail);
        }
        Ok(())
    }

    // Opcode 0x19.
    fn op_aux_pop_list_match(&mut self) -> OpcodeResult {
        use self::LiveValueTag::*;

        let arg = self.get_operand_value()?;
        let old_top = self.top;

        let mut key = self.deref(arg);
        let list = self.pop_serialized_list()?;

        loop {
            let (tag_key, val_key) = split_tagged_value(key);
            if tag_key != Pair {
                break;
            }
            let mut iter = list;
            let mut match_ = false;
            loop {
                let (tag_iter, val_iter) = split_tagged_value(iter);
                if tag_iter != Pair || match_ {
                    break;
                }
                if self.would_unify(
                    u16_to_tagged_reference(val_iter),
                    u16_to_tagged_reference(val_key)
                ) {
                    match_ = true;
                }
                iter = self.heap[val_iter as usize + 1]
            }
            if !match_ {
                return Err(RuntimeError::Fail)
            }
            key = self.heap[val_key as usize + 1];
        }
        self.top = old_top;
        Ok(())
    }

    // Opcode 0x1B.
    fn op_split_list(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_value()?;
        let arg_3 = self.get_operand_dest()?;
        let split = self.split_list(arg_1, arg_2)?;
        self.store_at_operand_dest(arg_3, split)
    }

    // Opcode 0x1C.
    fn op_stop(&mut self) -> OpcodeResult {
        self.cho = self.stc;
        Err(RuntimeError::Fail)
    }

    // Opcode 0x1D.
    fn op_push_stop(&mut self) -> OpcodeResult {
        let arg = self.get_operand_code()?;
        if self.aux + 2 > self.trl {
            return Err(RuntimeError::AuxHeapFull);
        }
        self.aux_heap[self.aux as usize] = self.stc;
        self.aux += 1;
        self.aux_heap[self.aux as usize] = self.sta;
        self.aux += 1;
        self.sta = self.aux;
        self.push_choice(0, arg)?;
        self.stc = self.cho;
        Ok(())
    }

    // Opcode 0x1E.
    fn op_pop_stop(&mut self) -> OpcodeResult {
        self.aux = self.sta;
        self.aux -= 1;
        self.sta = self.aux_heap[self.aux as usize];
        self.aux -= 1;
        self.stc = self.aux_heap[self.aux as usize];
        Ok(())
    }

    // Opcode 0x1F.
    fn op_split_word(&mut self) -> OpcodeResult {
        use self::LiveValueTag::*;

        let arg_1 = self.get_operand_value()?;
        let arg_1 = self.deref(arg_1);
        let arg_2 = self.get_operand_dest()?;
        let (tag, val) = split_tagged_value(arg_1);
        match tag {
            Character => {
                let result = self.create_pair(arg_1, TAGGED_EMPTY_LIST)?;
                self.store_at_operand_dest(arg_2, result)
            }
            Word => {
                let result = self.prepend_chars(arg_1, TAGGED_EMPTY_LIST)?;
                self.store_at_operand_dest(arg_2, result)
            }
            ExtendedWord => {
                let val_usize = val as usize;
                let part_1 = self.heap[val_usize];
                let (tag_part, _) = split_tagged_value(part_1);
                if tag_part == Pair {
                    self.store_at_operand_dest(arg_2, part_1)
                } else {
                    let part_2 = self.heap[val_usize + 1];
                    let result = self.prepend_chars(part_1, part_2)?;
                    self.store_at_operand_dest(arg_2, result)
                }
            }
            Number => {
                let mut i = val;
                let mut list = TAGGED_EMPTY_LIST;
                loop {
                    list = self.create_pair(u16_to_tagged_number(i % 10), list)?;
                    i /= 10;
                    if i == 0 {
                        break;
                    }
                }
                self.store_at_operand_dest(arg_2, list)
            }
            _ => Err(RuntimeError::Fail)
        }
    }

    // Opcode 0x9F.
    fn op_join_words(&mut self) -> OpcodeResult {
        use self::LiveValueTag::*;

        let arg_1 = self.get_operand_value()?;
        let arg_1 = self.deref(arg_1);
        let arg_2 = self.get_operand_dest()?;
        let (tag, val) = split_tagged_value(arg_1);
        let val = val as usize;
        if tag != Pair {
            return Err(RuntimeError::Fail);
        }
        let v1 = self.deref(self.heap[val]);
        let v2 = self.deref(self.heap[val + 1]);
        let (tag_v1, _) = split_tagged_value(v1);
        let result = if tag_v1 == Character && v2 == TAGGED_EMPTY_LIST {
            // Allow e.g. stop characters on their own.
            v1
        } else {
            self.parse_word(&self.wordlist_to_string(arg_1)?)?
        };
        self.store_at_operand_dest(arg_2, result)
    }

    // Opcode 0x20.
    fn op_load_word(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_index()?;
        let arg_3 = self.get_operand_dest()?;
        let val = self.read_field(arg_2, self.deref(arg_1));
        self.store_at_operand_dest(arg_3, val)
    }

    // Opcode 0xA0.
    fn op_load_word_0(&mut self) -> OpcodeResult {
        let arg_1 = 0;
        let arg_2 = self.get_operand_index()?;
        let arg_3 = self.get_operand_dest()?;
        let val = self.read_field(arg_2, self.deref(arg_1));
        self.store_at_operand_dest(arg_3, val)
    }

    // Opcode 0x21.
    fn op_load_byte(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_1 = self.deref(arg_1);
        let arg_2 = self.get_operand_index()?;
        let arg_3 = self.get_operand_dest()?;
        let flag = if arg_2 & 1 != 0 { 0 } else { 8 };
        let val = (self.read_field(arg_2 / 2, arg_1) >> flag) & 0xFF;
        self.store_at_operand_dest(arg_3, val)
    }

    // Opcode 0xA1.
    fn op_load_byte_0(&mut self) -> OpcodeResult {
        let arg_1 = 0;
        let arg_1 = self.deref(arg_1);
        let arg_2 = self.get_operand_index()?;
        let arg_3 = self.get_operand_dest()?;
        let flag = if (arg_2 & 1) != 0 { 0 } else { 8 };
        let val = (self.read_field(arg_2 / 2, arg_1) >> flag) & 0xFF;
        self.store_at_operand_dest(arg_3, val)
    }

    // Opcode 0x22.
    fn op_load_val(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_index()?;
        let arg_3 = self.get_operand_dest()?;
        let val = self.get_long_term(self.read_field(arg_2, self.deref(arg_1)))?;
        if val == 0 {
            return Err(RuntimeError::Fail);
        }
        self.store_at_operand_dest(arg_3, val)
    }

    // Opcode 0xA2.
    fn op_load_val_0(&mut self) -> OpcodeResult {
        let arg_2 = self.get_operand_index()?;
        let arg_3 = self.get_operand_dest()?;
        let val = self.get_long_term(self.read_field(arg_2, self.deref(0)))?;
        if val == 0 {
            return Err(RuntimeError::Fail);
        }
        self.store_at_operand_dest(arg_3, val)
    }

    // Opcode 0x24.
    fn op_store_word(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_1 = self.deref(arg_1);
        let arg_2 = self.get_operand_index()?;
        let arg_3 = self.get_operand_value()?;
        let addr = self.field_addr(arg_2, arg_1)? as usize;
        self.ram[addr] = arg_3;
        Ok(())
    }

    // Opcode 0xA4.
    fn op_store_word_0(&mut self) -> OpcodeResult {
        let arg_1 = 0;
        let arg_1 = self.deref(arg_1);
        let arg_2 = self.get_operand_index()?;
        let arg_3 = self.get_operand_value()?;
        let addr = self.field_addr(arg_2, arg_1)? as usize;
        self.ram[addr] = arg_3;
        Ok(())
    }

    // Opcode 0x25.
    fn op_store_byte(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_1 = self.deref(arg_1);
        let arg_2 = self.get_operand_index()?;
        let arg_3 = self.get_operand_value()?;
        let addr = self.field_addr(arg_2, arg_1)? as usize;
        let result = if (arg_2 & 1) != 0 {
            (self.ram[addr] & 0xFF00) | (arg_3 & 0xFF)
        } else {
            (self.ram[addr] & 0x00FF) | (arg_3 << 8)
        };
        self.ram[addr] = result;
        Ok(())
    }

    // Opcode 0xA5.
    fn op_store_byte_0(&mut self) -> OpcodeResult {
        let arg_1 = 0;
        let arg_1 = self.deref(arg_1);
        let arg_2 = self.get_operand_index()?;
        let arg_3 = self.get_operand_value()?;
        let addr = self.field_addr(arg_2, arg_1)? as usize;
        let result = if (arg_2 & 1) != 0 {
            (self.ram[addr] & 0xFF00) | (arg_3 & 0xFF)
        } else {
            (self.ram[addr] & 0x00FF) | (arg_3 << 8)
        };
        self.ram[addr] = result;
        Ok(())
    }

    // Opcode 0x26.
    fn op_store_val(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_1 = self.deref(arg_1);
        let arg_2 = self.get_operand_index()?;
        let arg_3 = self.get_operand_value()?;
        let (tag, _) = split_tagged_value(arg_1);
        if tag == LiveValueTag::Null || tag == LiveValueTag::Object || arg_3 != 0 {
            self.store_long_term(self.field_addr(arg_2, arg_1)?, arg_3)?
        }
        Ok(())
    }

    // Opcode 0xA6.
    fn op_store_val_0(&mut self) -> OpcodeResult {
        let arg_1 = self.deref(0);
        let arg_2 = self.get_operand_index()?;
        let arg_3 = self.get_operand_value()?;
        let (tag, _) = split_tagged_value(arg_1);
        if tag == LiveValueTag::Null || tag == LiveValueTag::Object || arg_3 != 0 {
            self.store_long_term(self.field_addr(arg_2, arg_1)?, arg_3)?
        }
        Ok(())
    }

    // Opcode 0x28.
    fn op_set_flag(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_1 = self.deref(arg_1);
        let arg_2 = self.get_operand_index()?;
        let bits_per_word = self.word_size() as u16 * 8;
        let addr = self.field_addr(arg_2 / bits_per_word, arg_1)? as usize;
        self.ram[addr] |= (1 << (bits_per_word - 1)) >> (arg_2 % bits_per_word);
        Ok(())
    }

    // Opcode 0xA8.
    fn op_set_flag_0(&mut self) -> OpcodeResult {
        let arg_1 = self.deref(0);
        let arg_2 = self.get_operand_index()?;
        let bits_per_word = self.word_size() as u16 * 8;
        let addr = self.field_addr(arg_2 / bits_per_word, arg_1)? as usize;
        self.ram[addr] |= (1 << (bits_per_word - 1)) >> (arg_2 % bits_per_word);
        Ok(())
    }

    // Opcode 0x29.
    fn op_reset_flag(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_1 = self.deref(arg_1);
        let arg_2 = self.get_operand_index()?;
        let bits_per_word = self.word_size() as u16 * 8;
        let addr = self.field_addr(arg_2 / bits_per_word, arg_1)? as usize;
        self.ram[addr] &= !((1 << (bits_per_word - 1)) >> (arg_2 % bits_per_word));
        Ok(())
    }

    // Opcode 0xA9.
    fn op_reset_flag_0(&mut self) -> OpcodeResult {
        let arg_1 = self.deref(0);
        let arg_2 = self.get_operand_index()?;
        let bits_per_word = self.word_size() as u16 * 8;
        let addr = self.field_addr(arg_2 / bits_per_word, arg_1)? as usize;
        self.ram[addr] &= !((1 << (bits_per_word - 1)) >> (arg_2 % bits_per_word));
        Ok(())
    }

    // Opcode 0x2D.
    fn op_unlink(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_1 = self.deref(arg_1);
        let arg_2 = self.get_operand_index()?;
        let arg_3 = self.get_operand_index()?;
        let arg_4 = self.get_operand_value()?;
        let addr = self.field_addr(arg_2, arg_1)?;
        self.unlink(addr, arg_3, arg_4)
    }

    // Opcode 0xAD.
    fn op_unlink_0(&mut self) -> OpcodeResult {
        let arg_1 = 0;
        let arg_1 = self.deref(arg_1);
        let arg_2 = self.get_operand_index()?;
        let arg_3 = self.get_operand_index()?;
        let arg_4 = self.get_operand_value()?;
        let addr = self.field_addr(arg_2, arg_1)?;
        self.unlink(addr, arg_3, arg_4)
    }

    // Opcode 0x2E.
    fn op_set_parent_value_value(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_value()?;
        self.set_parent(arg_1, arg_2)
    }

    // Opcode 0xAE.
    fn op_set_parent_vbyte_value(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_vbyte()? as u16;
        let arg_2 = self.get_operand_value()?;
        self.set_parent(arg_1, arg_2)
    }

    // Opcode 0x2F.
    fn op_set_parent_value_vbyte(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()? as u16;
        let arg_2 = self.get_operand_vbyte()? as u16;
        self.set_parent(arg_1, arg_2)
    }

    // Opcode 0xAF.
    fn op_set_parent_vbyte_vbyte(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_vbyte()? as u16;
        let arg_2 = self.get_operand_vbyte()? as u16;
        self.set_parent(arg_1, arg_2)
    }

    // Opcode 0x30.
    fn op_if_raw_eq(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_word()?;
        let arg_2 = self.get_operand_value()?;
        let arg_3 = self.get_operand_code()?;
        if arg_1 == arg_2 {
            self.inst = arg_3;
        }
        Ok(())
    }

    // Opcode 0xB0.
    fn op_if_raw_eq_0(&mut self) -> OpcodeResult {
        let arg_2 = self.get_operand_value()?;
        let arg_3 = self.get_operand_code()?;
        if 0 == arg_2 {
            self.inst = arg_3;
        }
        Ok(())
    }

    // Opcode 0x31.
    fn op_if_bound(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_code()?;
        let arg_1 = self.deref(arg_1);
        let (tag, _) = split_tagged_value(arg_1);
        if tag != LiveValueTag::Reference {
            self.inst = arg_2
        }
        Ok(())
    }

    // Opcode 0x32.
    fn op_if_empty(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_code()?;
        if self.deref(arg_1) == TAGGED_EMPTY_LIST {
            self.inst = arg_2
        }
        Ok(())
    }

    // Opcode 0x33.
    fn op_if_num(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_code()?;
        let (tag, _) = split_tagged_value(self.deref(arg_1));
        if tag == LiveValueTag::Number {
            self.inst = arg_2
        }
        Ok(())
    }

    // Opcode 0x34.
    fn op_if_pair(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_code()?;
        let (tag, _) = split_tagged_value(self.deref(arg_1));
        if tag == LiveValueTag::Pair {
            self.inst = arg_2
        }
        Ok(())
    }

    // Opcode 0x35.
    fn op_if_obj(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_code()?;
        let (tag, _) = split_tagged_value(self.deref(arg_1));
        if tag == LiveValueTag::Object {
            self.inst = arg_2
        }
        Ok(())
    }

    // Opcode 0x36.
    fn op_if_word(&mut self) -> OpcodeResult {
        use self::LiveValueTag::*;

        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_code()?;
        let (tag, _) = split_tagged_value(self.deref(arg_1));
        if matches!(tag, Word | Character | ExtendedWord) {
            self.inst = arg_2
        }
        Ok(())
    }

    // Opcode 0xB6.
    fn op_if_uword(&mut self) -> OpcodeResult {
        use self::LiveValueTag::*;

        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_code()?;
        let (tag, val) = split_tagged_value(self.deref(arg_1));
        let (tag_heap, _) = split_tagged_value(self.heap[val as usize]);
        if tag == ExtendedWord && tag_heap == Pair {
            self.inst = arg_2
        }
        Ok(())
    }

    // Opcode 0x37.
    fn op_if_unify(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_value()?;
        let arg_3 = self.get_operand_code()?;
        if self.would_unify(arg_1, arg_2) {
            self.inst = arg_3
        }
        Ok(())
    }

    // Opcode 0x38.
    fn op_if_gt(&mut self) -> OpcodeResult {
        use self::LiveValueTag::*;

        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_value()?;
        let arg_3 = self.get_operand_code()?;
        let (tag_1, value_1) = split_tagged_value(self.deref(arg_1));
        let (tag_2, value_2) = split_tagged_value(self.deref(arg_2));
        if tag_1 == Number && tag_2 == Number && value_1 > value_2 {
            self.inst = arg_3;
        }
        Ok(())
    }

    // Opcode 0x39.
    fn op_if_eq_word(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_word()?;
        let arg_2 = self.get_operand_value()?;
        let arg_3 = self.get_operand_code()?;
        if arg_1 == self.deref(arg_2) {
            self.inst = arg_3
        }
        Ok(())
    }

    // Opcode 0x40.
    fn op_ifn_raw_eq(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_word()?;
        let arg_2 = self.get_operand_value()?;
        let arg_3 = self.get_operand_code()?;
        if arg_1 != arg_2 {
            self.inst = arg_3;
        }
        Ok(())
    }

    // Opcode 0xC0.
    fn op_ifn_raw_eq_0(&mut self) -> OpcodeResult {
        let arg_2 = self.get_operand_value()?;
        let arg_3 = self.get_operand_code()?;
        if 0 != arg_2 {
            self.inst = arg_3;
        }
        Ok(())
    }

    // Opcode 0xB9.
    fn op_if_eq_vbyte(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_vbyte()? as u16;
        let arg_2 = self.get_operand_value()?;
        let arg_3 = self.get_operand_code()?;
        if arg_1 == self.deref(arg_2) {
            self.inst = arg_3
        }
        Ok(())
    }

    // Opcode 0x3A.
    fn op_if_mem_eq_value_value(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_index()?;
        let arg_3 = self.get_operand_value()?;
        let arg_4 = self.get_operand_code()?;

        if self.read_field(arg_2, self.deref(arg_1)) == arg_3 {
            self.inst = arg_4;
        }
        Ok(())
    }

    // Opcode 0xBA.
    fn op_if_mem_eq_0_value(&mut self) -> OpcodeResult {
        let arg_2 = self.get_operand_index()?;
        let arg_3 = self.get_operand_value()?;
        let arg_4 = self.get_operand_code()?;

        if self.read_field(arg_2, self.deref(0)) == arg_3 {
            self.inst = arg_4;
        }
        Ok(())
    }

    // Opcode 0x3B.
    fn op_if_flag(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_index()?;
        let arg_3 = self.get_operand_code()?;

        let bits_per_word = self.word_size() as u16 * 8;

        if (self.read_field(arg_2 / bits_per_word, self.deref(arg_1)) & ((1 << (bits_per_word - 1)) >> (arg_2 % bits_per_word))) != 0 {
            self.inst = arg_3;
        }
        Ok(())
    }

    // Opcode 0x3B.
    fn op_if_flag_0(&mut self) -> OpcodeResult {
        let arg_2 = self.get_operand_index()?;
        let arg_3 = self.get_operand_code()?;

        let bits_per_word = self.word_size() as u16 * 8;

        if (self.read_field(arg_2 / bits_per_word, self.deref(0)) & ((1 << (bits_per_word - 1)) >> (arg_2 % bits_per_word))) != 0 {
            self.inst = arg_3;
        }
        Ok(())
    }

    // Opcode 0x3C.
    fn op_if_cwl(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_code()?;
        if self.cwl != 0 {
            self.inst = arg_1;
        }
        Ok(())
    }

    // Opcode 0x3D.
    fn op_if_mem_eq_value_vbyte(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_index()?;
        let arg_3 = self.get_operand_vbyte()?;
        let arg_4 = self.get_operand_code()?;

        if self.read_field(arg_2, self.deref(arg_1)) == arg_3 as u16 {
            self.inst = arg_4;
        }
        Ok(())
    }

    // Opcode 0xBD.
    fn op_if_mem_eq_0_vbyte(&mut self) -> OpcodeResult {
        let arg_2 = self.get_operand_index()?;
        let arg_3 = self.get_operand_vbyte()?;
        let arg_4 = self.get_operand_code()?;

        if self.read_field(arg_2, self.deref(0)) == arg_3 as u16 {
            self.inst = arg_4;
        }
        Ok(())
    }

    // Opcode 0x41.
    fn op_ifn_bound(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_code()?;
        let arg_1 = self.deref(arg_1);
        let (tag, _) = split_tagged_value(arg_1);
        if tag == LiveValueTag::Reference {
            self.inst = arg_2
        }
        Ok(())
    }

    // Opcode 0x42.
    fn op_ifn_empty(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_code()?;
        if self.deref(arg_1) != TAGGED_EMPTY_LIST {
            self.inst = arg_2
        }
        Ok(())
    }

    // Opcode 0x43.
    fn op_ifn_num(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_code()?;
        let (tag, _) = split_tagged_value(self.deref(arg_1));
        if tag != LiveValueTag::Number {
            self.inst = arg_2
        }
        Ok(())
    }

    // Opcode 0x44.
    fn op_ifn_pair(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_code()?;
        let (tag, _) = split_tagged_value(self.deref(arg_1));
        if tag != LiveValueTag::Pair {
            self.inst = arg_2
        }
        Ok(())
    }

    // Opcode 0x45.
    fn op_ifn_obj(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_code()?;
        let (tag, _) = split_tagged_value(self.deref(arg_1));
        if tag != LiveValueTag::Object {
            self.inst = arg_2;
        }
        Ok(())
    }

    // Opcode 0x46.
    fn op_ifn_word(&mut self) -> OpcodeResult {
        use self::LiveValueTag::*;

        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_code()?;
        let (tag, _) = split_tagged_value(self.deref(arg_1));
        if !matches!(tag, Word | Character | ExtendedWord) {
            self.inst = arg_2
        }
        Ok(())
    }

    // Opcode 0xC6.
    fn op_ifn_uword(&mut self) -> OpcodeResult {
        use self::LiveValueTag::*;

        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_code()?;
        let (tag, val) = split_tagged_value(self.deref(arg_1));
        let (tag_heap, _) = split_tagged_value(self.heap[val as usize]);
        if tag != ExtendedWord || tag_heap != Pair {
            self.inst = arg_2
        }
        Ok(())
    }

    // Opcode 0x47.
    fn op_ifn_unify(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_value()?;
        let arg_3 = self.get_operand_code()?;
        if !self.would_unify(arg_1, arg_2) {
            self.inst = arg_3
        }
        Ok(())
    }

    // Opcode 0x48.
    fn op_ifn_gt(&mut self) -> OpcodeResult {
        use self::LiveValueTag::*;

        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_value()?;
        let arg_3 = self.get_operand_code()?;
        let (tag_1, value_1) = split_tagged_value(self.deref(arg_1));
        let (tag_2, value_2) = split_tagged_value(self.deref(arg_2));
        if tag_1 != Number || tag_2 != Number || value_1 <= value_2 {
            self.inst = arg_3;
        }
        Ok(())
    }

    // Opcode 0x49.
    fn op_ifn_eq_word(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_word()?;
        let arg_2 = self.get_operand_value()?;
        let arg_3 = self.get_operand_code()?;
        if arg_1 != self.deref(arg_2) {
            self.inst = arg_3
        }
        Ok(())
    }

    // Opcode 0xC9.
    fn op_ifn_eq_vbyte(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_vbyte()?;
        let arg_2 = self.get_operand_value()?;
        let arg_3 = self.get_operand_code()?;
        if arg_1 as u16 != self.deref(arg_2) {
            self.inst = arg_3
        }
        Ok(())
    }

    // Opcode 0x4A.
    fn op_ifn_mem_eq_value_value(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_index()?;
        let arg_3 = self.get_operand_value()?;
        let arg_4 = self.get_operand_code()?;

        if self.read_field(arg_2, self.deref(arg_1)) != arg_3 {
            self.inst = arg_4;
        }
        Ok(())
    }

    // Opcode 0xCA.
    fn op_ifn_mem_eq_0_value(&mut self) -> OpcodeResult {
        let arg_2 = self.get_operand_index()?;
        let arg_3 = self.get_operand_value()?;
        let arg_4 = self.get_operand_code()?;

        if self.read_field(arg_2, self.deref(0)) != arg_3 {
            self.inst = arg_4;
        }
        Ok(())
    }

    // Opcode 0x4B.
    fn op_ifn_flag(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_index()?;
        let arg_3 = self.get_operand_code()?;

        let bits_per_word = self.word_size() as u16 * 8;

        if (self.read_field(arg_2 / bits_per_word, self.deref(arg_1)) & ((1 << (bits_per_word - 1)) >> (arg_2 % bits_per_word))) == 0 {
            self.inst = arg_3;
        }
        Ok(())
    }

    // Opcode 0xCB.
    fn op_ifn_flag_0(&mut self) -> OpcodeResult {
        let arg_2 = self.get_operand_index()?;
        let arg_3 = self.get_operand_code()?;

        let bits_per_word = self.word_size() as u16 * 8;

        if (self.read_field(arg_2 / bits_per_word, self.deref(0)) & ((1 << (bits_per_word - 1)) >> (arg_2 % bits_per_word))) == 0 {
            self.inst = arg_3;
        }
        Ok(())
    }

    // Opcode 0x4C.
    fn op_ifn_cwl(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_code()?;
        if self.cwl == 0 {
            self.inst = arg_1;
        }
        Ok(())
    }

    // Opcode 0x4D.
    fn op_ifn_mem_eq_value_vbyte(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_index()?;
        let arg_3 = self.get_operand_vbyte()?;
        let arg_4 = self.get_operand_code()?;

        if self.read_field(arg_2, self.deref(arg_1)) != arg_3 as u16 {
            self.inst = arg_4;
        }
        Ok(())
    }

    // Opcode 0xCD.
    fn op_ifn_mem_eq_0_vbyte(&mut self) -> OpcodeResult {
        let arg_2 = self.get_operand_index()?;
        let arg_3 = self.get_operand_vbyte()?;
        let arg_4 = self.get_operand_code()?;

        if self.read_field(arg_2, self.deref(0)) != arg_3 as u16 {
            self.inst = arg_4;
        }
        Ok(())
    }

    // Opcode 0x50.
    fn op_add_raw(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_value()?;
        let arg_3 = self.get_operand_dest()?;
        self.store_at_operand_dest(arg_3, arg_1 + arg_2)
    }

    // Opcode 0xD0.
    fn op_inc_raw(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_dest()?;
        self.store_at_operand_dest(arg_2, arg_1 + 1)
    }

    // Opcode 0x51.
    fn op_sub_raw(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_value()?;
        let arg_3 = self.get_operand_dest()?;
        self.store_at_operand_dest(arg_3, arg_1 - arg_2)
    }

    // Opcode 0xD1.
    fn op_dec_raw(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_dest()?;
        self.store_at_operand_dest(arg_2, arg_1 - 1)
    }

    // Opcode 0x52.
    fn op_rand_raw(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_byte()?;
        let arg_2 = self.get_operand_dest()?;
        let result = self.random_number() % (arg_1 as u16 + 1);
        self.store_at_operand_dest(arg_2, result)
    }

    // Opcode 0x58.
    fn op_add_num(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_value()?;
        let arg_3 = self.get_operand_dest()?;
        let result = self.unbox_int(arg_1)? + self.unbox_int(arg_2)?;
        self.store_at_operand_dest(arg_3, self.box_int(result)?)
    }

    // Opcode 0xD8.
    fn op_inc_num(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_dest()?;
        let result = self.unbox_int(arg_1)? + 1;
        self.store_at_operand_dest(arg_2, self.box_int(result)?)
    }

    // Opcode 0x59.
    fn op_sub_num(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_value()?;
        let arg_3 = self.get_operand_dest()?;
        let result = self.unbox_int(arg_1)? - self.unbox_int(arg_2)?;
        self.store_at_operand_dest(arg_3, self.box_int(result)?)
    }

    // Opcode 0xD9.
    fn op_dec_num(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_dest()?;
        let result = self.unbox_int(arg_1)? - 1;
        self.store_at_operand_dest(arg_2, self.box_int(result)?)
    }

    // Opcode 0x5A.
    fn op_rand_num(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_value()?;
        let arg_3 = self.get_operand_dest()?;
        let start = self.unbox_int(arg_1)?;
        let range = self.unbox_int(arg_2)? - start + 1;
        if range < 1 {
            return Err(RuntimeError::Fail);
        }
        let result = start + self.random_number() % range;
        self.store_at_operand_dest(arg_3, result)
    }

    // Opcode 0x5B.
    fn op_mul_num(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_value()?;
        let arg_3 = self.get_operand_dest()?;
        let result = self.unbox_int(arg_1)? * self.unbox_int(arg_2)?;
        let result = result & 0x3FFF;
        self.store_at_operand_dest(arg_3, self.box_int(result)?)
    }

    // Opcode 0x5C.
    fn op_div_num(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_value()?;
        let arg_3 = self.get_operand_dest()?;
        let divisor = self.unbox_int(arg_2)?;
        if divisor == 0 {
            return Err(RuntimeError::Fail);
        }
        let result = self.unbox_int(arg_1)? / divisor;
        self.store_at_operand_dest(arg_3, self.box_int(result)?)
    }

    // Opcode 0x5D.
    fn op_mod_num(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_value()?;
        let arg_3 = self.get_operand_dest()?;
        let divisor = self.unbox_int(arg_2)?;
        if divisor == 0 {
            return Err(RuntimeError::Fail);
        }
        let result = self.unbox_int(arg_1)? % divisor;
        self.store_at_operand_dest(arg_3, self.box_int(result)?)
    }

    // Opcode 0x60.
    fn op_print_a_str_a(&mut self) -> OpcodeResult {
        use self::WhitespaceState::*;

        let arg = self.get_operand_string()?;
        if self.spc == Auto || self.spc == PendingSpace {
            self.frontend.space();
        }
        let string = self.decode_string(arg);
        self.frontend.string(&string);
        self.spc = Auto;
        Ok(())
    }

    // Opcode 0xE0.
    fn op_print_n_str_a(&mut self) -> OpcodeResult {
        use self::WhitespaceState::*;

        let arg = self.get_operand_string()?;
        if self.spc == PendingSpace {
            self.frontend.space();
        }
        let string = self.decode_string(arg);
        self.frontend.string(&string);
        self.spc = Auto;
        Ok(())
    }

    // Opcode 0x61.
    fn op_print_a_str_n(&mut self) -> OpcodeResult {
        use self::WhitespaceState::*;

        let arg = self.get_operand_string()?;
        if self.spc == Auto || self.spc == PendingSpace {
            self.frontend.space();
        }
        let string = self.decode_string(arg);
        self.frontend.string(&string);
        self.spc = NoSpace;
        Ok(())
    }

    // Opcode 0xE1.
    fn op_print_n_str_n(&mut self) -> OpcodeResult {
        use self::WhitespaceState::*;

        let arg = self.get_operand_string()?;
        if self.spc == PendingSpace {
            self.frontend.space();
        }
        let string = self.decode_string(arg);
        self.frontend.string(&string);
        self.spc = NoSpace;
        Ok(())
    }

    // Opcode 0x62.
    fn op_nospace(&mut self) -> OpcodeResult {
        if self.cwl == 0 && self.spc < WhitespaceState::NoSpace {
            self.spc = WhitespaceState::NoSpace;
        }
        Ok(())
    }

    // Opcode 0xE2.
    fn op_space(&mut self) -> OpcodeResult {
        if self.cwl == 0 && self.spc < WhitespaceState::PendingSpace {
            self.spc = WhitespaceState::PendingSpace;
        }
        Ok(())
    }

    // Opcode 0x63.
    fn op_line(&mut self) -> OpcodeResult {
        if self.cwl == 0 && self.spc < WhitespaceState::Line {
            self.frontend.newline();
            self.spc = WhitespaceState::Line;
        }
        Ok(())
    }

    // Opcode 0xE3.
    fn op_par(&mut self) -> OpcodeResult {
        if self.cwl == 0 && self.spc < WhitespaceState::Par {
            self.frontend.end_par();
            self.spc = WhitespaceState::Par;
        }
        Ok(())
    }

    // Opcode 0x64.
    fn op_space_n(&mut self) -> OpcodeResult {
        let arg = self.get_operand_value()?;
        if self.cwl == 0 {
            let (tag, val) = split_tagged_value(self.deref(arg));
            if tag == LiveValueTag::Number {
                self.frontend.space_n(val);
                self.spc = WhitespaceState::Space;
            }
        }
        Ok(())
    }

    // Opcode 0x65.
    fn op_print_val(&mut self) -> OpcodeResult {
        let arg = self.get_operand_value()?;
        let arg = self.deref(arg);
        if self.cwl == 0 {
            self.print_value(arg);
            Ok(())
        } else {
            self.push_serialized(arg)
        }
    }

    // Opcode 0x66.
    fn op_enter_div(&mut self) -> OpcodeResult {
        let arg = self.get_operand_index()?;
        if self.cwl == 0 {
            if self.n_span > 0 {
                return Err(RuntimeError::InvalidOutputState);
            }
            self.frontend.enter_div(arg);
            self.active_divs.push(arg);
            self.spc = WhitespaceState::Par;
        }
        Ok(())
    }

    // Opcode 0xE6.
    fn op_leave_div(&mut self) -> OpcodeResult {
        if self.cwl == 0 {
            // TODO: While it should never happen in a well-formed story, it will panic if there are no divs in the vec, which is bad.
            let style_class = self.active_divs.pop().unwrap();
            self.frontend.leave_div(style_class);
            self.spc = WhitespaceState::Par;
        }
        Ok(())
    }

    // Opcode 0x67.
    fn op_enter_status_0(&mut self) -> OpcodeResult {
        let arg_2 = self.get_operand_index()?;
        if self.in_status || self.n_span > 0 {
            return Err(RuntimeError::InvalidOutputState)
        }
        if self.cwl == 0 {
            self.frontend.enter_status(0, arg_2);
            self.spc = WhitespaceState::Par;
            self.in_status = true;
        }
        Ok(())
    }

    // Opcode 0xE7.
    fn op_leave_status(&mut self) -> OpcodeResult {
        if self.cwl == 0 {
            self.frontend.leave_status();
            self.spc = WhitespaceState::Par;
            self.in_status = false;
        }
        Ok(())
    }

    // Opcode 0x68.
    fn op_enter_link_res(&mut self) -> OpcodeResult {
        use self::WhitespaceState::*;

        let arg = self.get_operand_value()?;
        if self.cwl == 0 {
            if self.n_link == 0 {
                if self.spc == Auto || self.spc == PendingSpace {
                    self.frontend.space();
                }
                self.frontend.enter_link_res(self.deref(arg));
                self.spc = NoSpace;
            }
            self.n_link += 1;
            self.n_span += 1;
        }
        Ok(())
    }

    // Opcode 0xE8.
    fn op_leave_link_res(&mut self) -> OpcodeResult {
        if self.cwl == 0 {
            self.n_span -= 1;
            self.n_link -= 1;
            if self.n_link == 0 {
                self.frontend.leave_link_res();
            }
        }
        Ok(())
    }

    // Opcode 0x69.
    fn op_enter_link(&mut self) -> OpcodeResult {
        use self::WhitespaceState::*;
        use self::LiveValueTag::*;

        let arg = self.get_operand_value()?;
        if self.cwl == 0 {
            if self.n_link == 0 {
                if self.spc == Auto || self.spc == PendingSpace {
                    self.frontend.space();
                }
                let mut input = String::new();
                let mut arg = self.deref(arg);
                let (mut tag, mut value) = split_tagged_value(arg);
                while tag == Pair {
                    let w = self.deref(self.heap[value as usize]);
                    let (tag_w, value_w) = split_tagged_value(w);
                    if matches!(tag_w, Word | Character | ExtendedWord | Number) {
                        if !input.is_empty() {
                            input.push(' ');
                        }
                        input.push_str(&self.value_to_string(tag_w, value_w))
                    }
                    arg = self.deref(self.heap[value as usize + 1]);
                    let (new_tag, new_value) = split_tagged_value(arg);
                    tag = new_tag;
                    value = new_value;
                }
                self.frontend.enter_link(&input);
                self.spc = NoSpace;
            }
            self.n_link += 1;
            self.n_span += 1;
        }
        Ok(())
    }

    // Opcode 0xE9.
    fn op_leave_link(&mut self) -> OpcodeResult {
        if self.cwl == 0 {
            self.n_span -= 1;
            self.n_link -= 1;
            if self.n_link == 0 {
                self.frontend.leave_link();
            }
        }
        Ok(())
    }

    // Opcode 0x6A.
    fn op_enter_self_link(&mut self) -> OpcodeResult {
        use self::WhitespaceState::*;

        if self.cwl == 0 {
            if self.n_link == 0 {
                if self.spc == Auto || self.spc == PendingSpace {
                    self.frontend.space();
                }
                self.frontend.enter_self_link();
                self.spc = Space;
            }
            self.n_link += 1;
            self.n_span += 1;
        }
        Ok(())
    }

    // Opcode 0xEA.
    fn op_leave_self_link(&mut self) -> OpcodeResult {
        if self.cwl == 0 {
            self.n_span -= 1;
            self.n_link -= 1;
            if self.n_link == 0 {
                self.frontend.leave_self_link();
            }
        }
        Ok(())
    }

    // Opcode 0x6B.
    fn op_set_style(&mut self) -> OpcodeResult {
        use self::WhitespaceState::*;

        let arg = self.get_operand_byte()?;
        if self.cwl == 0 {
            if self.spc == Auto || self.spc == PendingSpace {
                self.frontend.space();
            }
            self.frontend.set_style(arg.into());
            self.spc = Space;
        }
        Ok(())
    }

    // Opcode 0xEB.
    fn op_reset_style(&mut self) -> OpcodeResult {
        let arg = self.get_operand_byte()?;
        if self.cwl == 0 {
            self.frontend.reset_style(arg.into());
        }
        Ok(())
    }

    // Opcode 0x6C.
    fn op_embed_res(&mut self) -> OpcodeResult {
        let arg = self.get_operand_value()?;
        if self.cwl == 0 {
            self.frontend.embed_res(self.deref(arg));
        }
        Ok(())
    }

    // Opcode 0xEC.
    fn op_can_embed_res(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_dest()?;
        let result = self.frontend.can_embed_res(self.deref(arg_1));
        self.store_at_operand_dest(arg_2, result as u16)
    }

    // Opcode 0x6D.
    fn op_progress(&mut self) -> OpcodeResult {
        use self::LiveValueTag::*;

        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_value()?;
        if self.cwl == 0 {
            let (tag_1, val_1) = split_tagged_value(self.deref(arg_1));
            let (tag_2, val_2) = split_tagged_value(self.deref(arg_2));
            if tag_1 == Number && tag_2 == Number {
                self.frontend.progress_bar(val_1, val_2);
            }
        }
        Ok(())
    }

    // Opcode 0x6E.
    fn op_enter_span(&mut self) -> OpcodeResult {
        let arg = self.get_operand_index()?;
        if self.cwl == 0 {
            if self.spc == WhitespaceState::Auto || self.spc == WhitespaceState::PendingSpace {
                self.frontend.space();
            }
            self.frontend.enter_span(arg);
            self.spc = WhitespaceState::NoSpace;
            self.n_span += 1;
        }
        Ok(())
    }

    // Opcode 0xEE.
    fn op_leave_span(&mut self) -> OpcodeResult {
        if self.cwl == 0 {
            self.frontend.leave_span();
            self.spc = WhitespaceState::Auto;
            self.n_span -= 1;
        }
        Ok(())
    }

    // Opcode 0x6F.
    fn op_enter_status(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_byte()?;
        let arg_2 = self.get_operand_index()?;
        if self.in_status || self.n_span > 0 {
            return Err(RuntimeError::InvalidOutputState)
        }
        if self.cwl == 0 {
            self.frontend.enter_status(arg_1 as u16, arg_2);
            self.spc = WhitespaceState::Par;
            self.in_status = true;
        }
        Ok(())
    }

    // Opcode 0x70.
    fn op_ext0(&mut self) -> OpcodeResult {
        use self::WhitespaceState::*;

        let arg = self.get_operand_byte()?;
        match arg {
            // quit.
            0x00 => {
                self.status = Status::Stopped;
                self.frontend.sync();
            }
            // restart.
            0x01 => {
                self.restart();
            }
            // restore.
            0x02 => {
                self.frontend.sync();
                self.status = Status::AwaitingRestore;
            }
            // undo.
            0x03 => {
                // TODO: Do nothing instead of failing if we cannot undo for reasons other than undoing past the first turn (e.g. if one day undos are limited, there could be no more undos saved).
                let state = self.undo_states.pop().ok_or(RuntimeError::Fail)?;
                self.restore_state(&state);
                for &div in &self.active_divs {
                    self.frontend.enter_div(div);
                }
            }
            // unstyle.
            0x04 => if self.cwl == 0 {
                self.frontend.unstyle();
            }
            // print_serial.
            0x05 => if self.cwl == 0 {
                if self.spc == Auto || self.spc == PendingSpace {
                    self.frontend.space();
                }
                let serial = self.serial_number().iter()
                    .map(|&byte| byte as char)
                    .collect::<String>();
                self.frontend.string(&serial);
                self.spc = Auto;
            }
            // clear.
            0x06 => if self.cwl == 0 {
                if self.in_status || self.n_span > 0 {
                    return Err(RuntimeError::InvalidOutputState);
                }
                self.frontend.clear();
            }
            // clear_all
            0x07 => if self.cwl == 0 {
                if self.in_status || self.n_span > 0 {
                    return Err(RuntimeError::InvalidOutputState);
                }
                self.frontend.clear_all()
            }
            // script_on.
            0x08 => if !self.frontend.script_on() {
                return Err(RuntimeError::Fail);
            }
            // script_off
            0x09 => self.frontend.script_off(),
            // trace_on.
            0x0A => self.trace = true,
            // trace_off.
            0x0B => self.trace = false,
            // inc_cwl.
            0x0C => self.cwl += 1,
            // dec_cwl.
            0x0D => self.cwl -= 1,
            // uppercase.
            0x0E => if self.cwl == 0 { self.uppercase = true },
            // clear_links.
            0x0F => self.frontend.clear_links(),
            // clear_old.
            0x10 => {
                if self.n_span > 0 {
                    return Err(RuntimeError::InvalidOutputState);
                }
                self.frontend.clear_old()
            }
            // clear_div.
            0x11 => self.frontend.clear_div(),
            // We only show a warning with an invalid ext0.
            n => self.frontend.warning(&format!("Unimplemented ext0 branch: {}.", n))
        }
        Ok(())
    }

    // Opcode 0xF2.
    fn op_save(&mut self) -> OpcodeResult {
        let arg = self.get_operand_code()?;
        if self.in_status || self.n_span > 0 {
            return Err(RuntimeError::InvalidOutputState);
        }
        let data = self.save_state(Some(arg)).into_aasave(&self.head, &self.initial_state);
        self.status = Status::AwaitingSave(data);
        // The end of the opcode is handled in `send_save_result` so we just return Ok.
        Ok(())
    }

    // Opcode 0xF2.
    fn op_save_undo(&mut self) -> OpcodeResult {
        let arg = self.get_operand_code()?;
        if self.in_status || self.n_span > 0 {
            return Err(RuntimeError::InvalidOutputState);
        }
        // TODO: Limit number of saved turns.
        self.undo_states.push(self.save_state(Some(arg)));
        // Saving an undo can never fail.
        Ok(())
    }

    // Opcode 0x73.
    fn op_get_input(&mut self) -> OpcodeResult {
        use self::WhitespaceState::*;

        let arg = self.get_operand_dest()?;
        if self.spc == Auto || self.spc == PendingSpace {
            self.frontend.space();
        }
        self.frontend.sync();
        self.status = Status::AwaitingInput;
        // The end of the opcode is handled in `send_input`
        // so we just keep track of the destination and return Ok.
        self.input_dest = arg;
        Ok(())
    }

    // Opcode 0xF3.
    fn op_get_key(&mut self) -> OpcodeResult {
        use self::WhitespaceState::*;

        let arg = self.get_operand_dest()?;
        if self.spc == Auto || self.spc == PendingSpace {
            self.frontend.space();
        }
        self.frontend.sync();
        self.status = Status::AwaitingKey;
        // The end of the opcode is handled in `send_key`
        // so we just keep track of the destination and return Ok.
        self.input_dest = arg;
        Ok(())
    }

    // Opcode 0x74.
    fn op_vm_info(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_byte()?;
        let arg_2 = self.get_operand_dest()?;
        let result = match arg_1 {
            // Peak heap.
            0x00 => self.box_int(self.heap.iter().filter(|&&val| val != 0x3F3F).count() as u16)?,
            // Peak auxiliary heap.
            0x01 => self.box_int(self.aux_heap.iter().filter(|&&val| val != 0x3F3F).count() as u16)?,
            // Peak long-term heap.
            0x02 => self.box_int(self.ram[self.ltb as usize..].iter().filter(|&&val| val != 0x3F3F).count() as u16)?,
            // Supports undo.
            0x40 => 1,
            // Supports save/restore.
            // TODO: Or maybe add it as an option when instantiating the interpreter.
            0x41 => 1,
            // Supports hyperlinks.
            0x42 => self.frontend.supports_hyperlinks() as u16,
            // Supports quit.
            // TODO: Or maybe add it as an option when instantiating the interpreter.
            0x43 => 1,
            0x60 => self.frontend.supports_top_status() as u16,
            0x61 => self.frontend.supports_inline_status() as u16,
            // Default to 0 when checking feature support.
            _ if arg_1 >= 0x40 => 0,
            _ => return Err(FatalError::UnimplementedVmInfo(arg_1).into())
        };
        self.store_at_operand_dest(arg_2, result)
    }

    // Opcode 0x78.
    fn op_set_idx(&mut self) -> OpcodeResult {
        let arg = self.get_operand_value()?;
        self.registers[0x3F] = self.deref(arg);
        let (tag, value) = split_tagged_value(self.registers[0x3F]);
        if tag == LiveValueTag::ExtendedWord {
            self.registers[0x3F] = self.heap[value as usize]
        }
        Ok(())
    }

    // Opcode 0x79.
    fn op_check_eq_word(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_word()?;
        let arg_2 = self.get_operand_code()?;
        if self.registers[0x3F] == arg_1 {
            self.inst = arg_2;
        }
        Ok(())
    }

    // Opcode 0x7A.
    fn op_check_gt_eq_word(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_word()?;
        let arg_2 = self.get_operand_code()?;
        let arg_3 = self.get_operand_code()?;
        match self.registers[0x3F].cmp(&arg_1) {
            Ordering::Greater => self.inst = arg_2,
            Ordering::Equal => self.inst = arg_3,
            Ordering::Less => (),
        }
        Ok(())
    }

    // Opcode 0xFA.
    fn op_check_gt_eq_vbyte(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_vbyte()?;
        let arg_2 = self.get_operand_code()?;
        let arg_3 = self.get_operand_code()?;
        match self.registers[0x3F].cmp(&(arg_1 as u16)) {
            Ordering::Greater => self.inst = arg_2,
            Ordering::Equal => self.inst = arg_3,
            Ordering::Less => (),
        }
        Ok(())
    }

    // Opcode 0x7B.
    fn op_check_gt_value(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_value()?;
        let arg_2 = self.get_operand_code()?;
        if self.registers[0x3F] > arg_1 {
            self.inst = arg_2;
        }
        Ok(())
    }

    // Opcode 0xFB.
    fn op_check_gt_byte(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_byte()?;
        let arg_2 = self.get_operand_code()?;
        if self.registers[0x3F] > arg_1 as u16 {
            self.inst = arg_2;
        }
        Ok(())
    }

    // Opcode 0x7C.
    fn op_check_wordmap(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_index()? as usize;
        let arg_2 = self.get_operand_code()?;
        let idx = self.registers[0x3F];
        let map_addr = read_u16(&self.maps, 2 + 2 * arg_1) as usize;
        let map_length = 4 * read_u16(&self.maps, map_addr) as usize; // Each entry is 4 bytes.
        // TODO:
        // Using `find_map` on the chunk iterator does a linear search.
        // Since the spec states that the map is sorted by index, we could do a binary search, but we'd need `as_chunks`, which is not stable yet.
        // (Or we could implement the binary search ourselves, if we really wanted to...)
        let value = self.maps[map_addr + 2..map_addr + 2 + map_length]
            .chunks(4)
            .find_map(|x| {
                if u16::from_be_bytes([x[0], x[1]]) != idx {
                    None
                } else {
                    Some(u16::from_be_bytes([x[2], x[3]]))
                }
            });

        match value {
            // The word was not in the table, and should not match anything.
            None => self.inst = arg_2,
            // The word is a wildcard, so don't jump.
            Some(0) => (),
            // The word matches a limited number of objects. Push them and jump to enforce that.
            Some(x) => {
                if x >= 0xE000 {
                    let obj = x - 0xE000;
                    if self.aux >= self.trl {
                        return Err(RuntimeError::AuxHeapFull);
                    }
                    self.aux_heap[self.aux as usize] = obj;
                    self.aux += 1;
                    self.inst = arg_2;
                } else {
                    let mut pos = x as usize;
                    loop {
                        let byte = self.maps[pos];
                        let obj = if byte == 0 {
                            break;
                        } else if byte > 0xDF {
                            pos += 1;
                            u16::from_be_bytes([byte & 0x1F, self.maps[pos]])
                        } else {
                            byte as u16
                        };
                        if self.aux >= self.trl {
                            return Err(RuntimeError::AuxHeapFull);
                        }
                        self.aux_heap[self.aux as usize] = obj;
                        self.aux += 1;
                        pos += 1;
                    }
                }
                self.inst = arg_2;
            }
        }
        Ok(())
    }

    // Opcode 0x7D.
    fn op_check_eq_2_word(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_word()?;
        let arg_2 = self.get_operand_word()?;
        let arg_3 = self.get_operand_code()?;
        let idx = self.registers[0x3F];
        if idx == arg_1 || idx == arg_2 {
            self.inst = arg_3;
        }
        Ok(())
    }

    // Opcode 0x7D.
    fn op_check_eq_2_vbyte(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_vbyte()? as u16;
        let arg_2 = self.get_operand_vbyte()? as u16;
        let arg_3 = self.get_operand_code()?;
        let idx = self.registers[0x3F];
        if idx == arg_1 || idx == arg_2 {
            self.inst = arg_3;
        }
        Ok(())
    }

    // Opcode 0xF9.
    fn op_check_eq_vbyte(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_vbyte()?;
        let arg_2 = self.get_operand_code()?;
        if self.registers[0x3F] == arg_1 as u16 {
            self.inst = arg_2;
        }
        Ok(())
    }

    fn op_tracepoint(&mut self) -> OpcodeResult {
        let arg_1 = self.get_operand_string()?;
        let arg_2 = self.get_operand_string()?;
        let arg_3 = self.get_operand_string()?;
        let arg_4 = self.get_operand_word()?;
        if self.trace {
            let mut string = self.decode_string(arg_1);
            string.push('(');
            let arg_2 = self.decode_string(arg_2);
            let mut reg = 0;
            for ch in arg_2.chars() {
                if ch == '$' {
                    let (tag, val) = split_tagged_value(self.registers[reg]);
                    reg += 1;
                    string.push_str(&self.value_to_string(tag, val));
                } else {
                    string.push(ch);
                }
            }
            string.push(')');
            string.push(' ');
            string.push_str(&self.decode_string(arg_3));
            string.push(':');
            string.push_str(&arg_4.to_string());
            self.frontend.trace(&string);
        }
        Ok(())
    }
}
